export const data = [
    {
        "OMRID": 3100770,
        "name": "Love Everett",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 105,
            "percentage": 98.18,
            "rank": 85
        },
        "chemistry": {
            "marks": 107,
            "percentage": 99.81,
            "rank": 30
        },
        "maths": {
            "marks": 107,
            "percentage": 95.31,
            "rank": 88
        },
        "total": {
            "marks": 305,
            "percentage": 99.17,
            "rank": 83
        }
    },
    {
        "OMRID": 1099651,
        "name": "Myra Barron",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 110,
            "percentage": 96.96,
            "rank": 80
        },
        "chemistry": {
            "marks": 118,
            "percentage": 99.57,
            "rank": 71
        },
        "maths": {
            "marks": 109,
            "percentage": 98.02,
            "rank": 73
        },
        "total": {
            "marks": 347,
            "percentage": 99.58,
            "rank": 95
        }
    },
    {
        "OMRID": 8529542,
        "name": "Cathleen Bradley",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 118,
            "percentage": 97.71,
            "rank": 22
        },
        "chemistry": {
            "marks": 120,
            "percentage": 96.52,
            "rank": 58
        },
        "maths": {
            "marks": 103,
            "percentage": 99.75,
            "rank": 70
        },
        "total": {
            "marks": 360,
            "percentage": 98.88,
            "rank": 26
        }
    },
    {
        "OMRID": 5474793,
        "name": "Diaz George",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 105,
            "percentage": 96.04,
            "rank": 40
        },
        "chemistry": {
            "marks": 113,
            "percentage": 97.08,
            "rank": 37
        },
        "maths": {
            "marks": 114,
            "percentage": 96.45,
            "rank": 60
        },
        "total": {
            "marks": 321,
            "percentage": 99.98,
            "rank": 50
        }
    },
    {
        "OMRID": 6313974,
        "name": "Jerri Cruz",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 111,
            "percentage": 96.45,
            "rank": 65
        },
        "chemistry": {
            "marks": 106,
            "percentage": 96.21,
            "rank": 4
        },
        "maths": {
            "marks": 110,
            "percentage": 98.53,
            "rank": 95
        },
        "total": {
            "marks": 315,
            "percentage": 97.49,
            "rank": 55
        }
    },
    {
        "OMRID": 5086265,
        "name": "Rivas Whitehead",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 115,
            "percentage": 97.17,
            "rank": 1
        },
        "chemistry": {
            "marks": 103,
            "percentage": 97.28,
            "rank": 2
        },
        "maths": {
            "marks": 120,
            "percentage": 96.43,
            "rank": 77
        },
        "total": {
            "marks": 303,
            "percentage": 98.34,
            "rank": 83
        }
    },
    {
        "OMRID": 5827776,
        "name": "Katheryn Nunez",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 100,
            "percentage": 96.22,
            "rank": 24
        },
        "chemistry": {
            "marks": 119,
            "percentage": 96.84,
            "rank": 52
        },
        "maths": {
            "marks": 101,
            "percentage": 97,
            "rank": 23
        },
        "total": {
            "marks": 339,
            "percentage": 99.23,
            "rank": 23
        }
    },
    {
        "OMRID": 2648897,
        "name": "Genevieve Lang",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 115,
            "percentage": 97.35,
            "rank": 20
        },
        "chemistry": {
            "marks": 115,
            "percentage": 95.7,
            "rank": 56
        },
        "maths": {
            "marks": 103,
            "percentage": 97.46,
            "rank": 62
        },
        "total": {
            "marks": 306,
            "percentage": 98.77,
            "rank": 73
        }
    },
    {
        "OMRID": 8329628,
        "name": "Sweet Barnett",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 105,
            "percentage": 95.31,
            "rank": 48
        },
        "chemistry": {
            "marks": 112,
            "percentage": 99.03,
            "rank": 8
        },
        "maths": {
            "marks": 116,
            "percentage": 97.62,
            "rank": 40
        },
        "total": {
            "marks": 330,
            "percentage": 98.62,
            "rank": 55
        }
    },
    {
        "OMRID": 1959929,
        "name": "Stacy Mccarty",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 105,
            "percentage": 97.21,
            "rank": 22
        },
        "chemistry": {
            "marks": 102,
            "percentage": 95.89,
            "rank": 49
        },
        "maths": {
            "marks": 118,
            "percentage": 97.57,
            "rank": 62
        },
        "total": {
            "marks": 331,
            "percentage": 98.99,
            "rank": 21
        }
    },
    {
        "OMRID": 25935310,
        "name": "Randolph Webster",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 104,
            "percentage": 98.17,
            "rank": 31
        },
        "chemistry": {
            "marks": 104,
            "percentage": 97.56,
            "rank": 4
        },
        "maths": {
            "marks": 100,
            "percentage": 95.62,
            "rank": 12
        },
        "total": {
            "marks": 351,
            "percentage": 98.58,
            "rank": 77
        }
    },
    {
        "OMRID": 90679211,
        "name": "Jefferson Bright",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 119,
            "percentage": 97.31,
            "rank": 5
        },
        "chemistry": {
            "marks": 102,
            "percentage": 97.66,
            "rank": 67
        },
        "maths": {
            "marks": 119,
            "percentage": 98.48,
            "rank": 64
        },
        "total": {
            "marks": 340,
            "percentage": 96.86,
            "rank": 77
        }
    },
    {
        "OMRID": 20183712,
        "name": "Mai Atkinson",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 120,
            "percentage": 97.48,
            "rank": 97
        },
        "chemistry": {
            "marks": 111,
            "percentage": 98.88,
            "rank": 49
        },
        "maths": {
            "marks": 101,
            "percentage": 96.7,
            "rank": 67
        },
        "total": {
            "marks": 320,
            "percentage": 98.77,
            "rank": 61
        }
    },
    {
        "OMRID": 19233113,
        "name": "Morrison Pope",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 115,
            "percentage": 96.95,
            "rank": 55
        },
        "chemistry": {
            "marks": 119,
            "percentage": 98.02,
            "rank": 55
        },
        "maths": {
            "marks": 100,
            "percentage": 98.73,
            "rank": 57
        },
        "total": {
            "marks": 347,
            "percentage": 96.33,
            "rank": 58
        }
    },
    {
        "OMRID": 96468514,
        "name": "Dickerson Holland",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 118,
            "percentage": 98.43,
            "rank": 89
        },
        "chemistry": {
            "marks": 107,
            "percentage": 95.54,
            "rank": 48
        },
        "maths": {
            "marks": 110,
            "percentage": 95.34,
            "rank": 73
        },
        "total": {
            "marks": 353,
            "percentage": 96.49,
            "rank": 51
        }
    },
    {
        "OMRID": 68364715,
        "name": "Angela Clements",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 106,
            "percentage": 95.05,
            "rank": 22
        },
        "chemistry": {
            "marks": 113,
            "percentage": 95.88,
            "rank": 78
        },
        "maths": {
            "marks": 103,
            "percentage": 99.6,
            "rank": 59
        },
        "total": {
            "marks": 312,
            "percentage": 97.35,
            "rank": 30
        }
    },
    {
        "OMRID": 79838516,
        "name": "Caitlin Wise",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 113,
            "percentage": 97.14,
            "rank": 71
        },
        "chemistry": {
            "marks": 109,
            "percentage": 95.32,
            "rank": 98
        },
        "maths": {
            "marks": 114,
            "percentage": 99.56,
            "rank": 56
        },
        "total": {
            "marks": 350,
            "percentage": 97.09,
            "rank": 44
        }
    },
    {
        "OMRID": 86675417,
        "name": "Ramsey Harper",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 112,
            "percentage": 96.49,
            "rank": 46
        },
        "chemistry": {
            "marks": 111,
            "percentage": 98.25,
            "rank": 24
        },
        "maths": {
            "marks": 113,
            "percentage": 97.91,
            "rank": 62
        },
        "total": {
            "marks": 316,
            "percentage": 96.25,
            "rank": 38
        }
    },
    {
        "OMRID": 58790518,
        "name": "Sanchez Mclaughlin",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 118,
            "percentage": 99.79,
            "rank": 6
        },
        "chemistry": {
            "marks": 100,
            "percentage": 99.11,
            "rank": 75
        },
        "maths": {
            "marks": 107,
            "percentage": 96.83,
            "rank": 66
        },
        "total": {
            "marks": 345,
            "percentage": 99.71,
            "rank": 87
        }
    },
    {
        "OMRID": 80631919,
        "name": "Hewitt Ashley",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 100,
            "percentage": 95,
            "rank": 59
        },
        "chemistry": {
            "marks": 116,
            "percentage": 96.46,
            "rank": 7
        },
        "maths": {
            "marks": 119,
            "percentage": 96.6,
            "rank": 7
        },
        "total": {
            "marks": 354,
            "percentage": 95.91,
            "rank": 56
        }
    },
    {
        "OMRID": 42430820,
        "name": "Elena Pacheco",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 107,
            "percentage": 98.42,
            "rank": 69
        },
        "chemistry": {
            "marks": 112,
            "percentage": 97.17,
            "rank": 37
        },
        "maths": {
            "marks": 119,
            "percentage": 95.09,
            "rank": 27
        },
        "total": {
            "marks": 352,
            "percentage": 98.83,
            "rank": 48
        }
    },
    {
        "OMRID": 44011221,
        "name": "Kerry Kline",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 108,
            "percentage": 99.27,
            "rank": 22
        },
        "chemistry": {
            "marks": 115,
            "percentage": 96.22,
            "rank": 86
        },
        "maths": {
            "marks": 112,
            "percentage": 96.76,
            "rank": 53
        },
        "total": {
            "marks": 323,
            "percentage": 98.69,
            "rank": 82
        }
    },
    {
        "OMRID": 46491122,
        "name": "Rosie Rutledge",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 101,
            "percentage": 95.13,
            "rank": 28
        },
        "chemistry": {
            "marks": 113,
            "percentage": 98.86,
            "rank": 10
        },
        "maths": {
            "marks": 110,
            "percentage": 96.39,
            "rank": 7
        },
        "total": {
            "marks": 346,
            "percentage": 98.99,
            "rank": 43
        }
    },
    {
        "OMRID": 26493223,
        "name": "Leah Coleman",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 100,
            "percentage": 98.3,
            "rank": 31
        },
        "chemistry": {
            "marks": 102,
            "percentage": 97.68,
            "rank": 44
        },
        "maths": {
            "marks": 105,
            "percentage": 97.35,
            "rank": 63
        },
        "total": {
            "marks": 360,
            "percentage": 98.31,
            "rank": 86
        }
    },
    {
        "OMRID": 68402724,
        "name": "Aileen Byers",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 117,
            "percentage": 97.5,
            "rank": 4
        },
        "chemistry": {
            "marks": 113,
            "percentage": 95.73,
            "rank": 98
        },
        "maths": {
            "marks": 115,
            "percentage": 97.9,
            "rank": 68
        },
        "total": {
            "marks": 346,
            "percentage": 97.21,
            "rank": 95
        }
    },
    {
        "OMRID": 78943425,
        "name": "Peterson Walsh",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 115,
            "percentage": 98.52,
            "rank": 85
        },
        "chemistry": {
            "marks": 119,
            "percentage": 95.69,
            "rank": 32
        },
        "maths": {
            "marks": 120,
            "percentage": 97.04,
            "rank": 21
        },
        "total": {
            "marks": 336,
            "percentage": 96.28,
            "rank": 78
        }
    },
    {
        "OMRID": 40922526,
        "name": "Doris Camacho",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 107,
            "percentage": 99.13,
            "rank": 45
        },
        "chemistry": {
            "marks": 113,
            "percentage": 95.3,
            "rank": 18
        },
        "maths": {
            "marks": 115,
            "percentage": 97.7,
            "rank": 91
        },
        "total": {
            "marks": 353,
            "percentage": 96.46,
            "rank": 4
        }
    },
    {
        "OMRID": 16265127,
        "name": "Ochoa Sims",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 110,
            "percentage": 95.62,
            "rank": 79
        },
        "chemistry": {
            "marks": 111,
            "percentage": 96.87,
            "rank": 45
        },
        "maths": {
            "marks": 101,
            "percentage": 95.32,
            "rank": 54
        },
        "total": {
            "marks": 305,
            "percentage": 99.15,
            "rank": 47
        }
    },
    {
        "OMRID": 94752628,
        "name": "Sloan Richardson",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 114,
            "percentage": 97.8,
            "rank": 30
        },
        "chemistry": {
            "marks": 106,
            "percentage": 99.7,
            "rank": 34
        },
        "maths": {
            "marks": 107,
            "percentage": 96.66,
            "rank": 74
        },
        "total": {
            "marks": 343,
            "percentage": 99.53,
            "rank": 59
        }
    },
    {
        "OMRID": 39879129,
        "name": "William Waller",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 109,
            "percentage": 95.29,
            "rank": 53
        },
        "chemistry": {
            "marks": 112,
            "percentage": 98.16,
            "rank": 15
        },
        "maths": {
            "marks": 119,
            "percentage": 98.9,
            "rank": 14
        },
        "total": {
            "marks": 323,
            "percentage": 99.91,
            "rank": 78
        }
    },
    {
        "OMRID": 48572130,
        "name": "Figueroa Aguilar",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 104,
            "percentage": 99.89,
            "rank": 28
        },
        "chemistry": {
            "marks": 111,
            "percentage": 96.84,
            "rank": 60
        },
        "maths": {
            "marks": 119,
            "percentage": 98.81,
            "rank": 68
        },
        "total": {
            "marks": 317,
            "percentage": 97.29,
            "rank": 21
        }
    },
    {
        "OMRID": 92535331,
        "name": "Kristie Quinn",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 102,
            "percentage": 95.49,
            "rank": 5
        },
        "chemistry": {
            "marks": 104,
            "percentage": 97.73,
            "rank": 75
        },
        "maths": {
            "marks": 116,
            "percentage": 97.24,
            "rank": 29
        },
        "total": {
            "marks": 359,
            "percentage": 99.07,
            "rank": 13
        }
    },
    {
        "OMRID": 51474032,
        "name": "Walton Harvey",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 104,
            "percentage": 99.41,
            "rank": 98
        },
        "chemistry": {
            "marks": 110,
            "percentage": 95.66,
            "rank": 13
        },
        "maths": {
            "marks": 101,
            "percentage": 98.64,
            "rank": 81
        },
        "total": {
            "marks": 317,
            "percentage": 97.5,
            "rank": 80
        }
    },
    {
        "OMRID": 45980733,
        "name": "Luz Padilla",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 107,
            "percentage": 98.29,
            "rank": 14
        },
        "chemistry": {
            "marks": 118,
            "percentage": 96.56,
            "rank": 17
        },
        "maths": {
            "marks": 113,
            "percentage": 99.01,
            "rank": 8
        },
        "total": {
            "marks": 340,
            "percentage": 96.14,
            "rank": 55
        }
    },
    {
        "OMRID": 93998634,
        "name": "Chapman Owen",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 119,
            "percentage": 98.5,
            "rank": 44
        },
        "chemistry": {
            "marks": 112,
            "percentage": 99.98,
            "rank": 18
        },
        "maths": {
            "marks": 103,
            "percentage": 97.54,
            "rank": 66
        },
        "total": {
            "marks": 358,
            "percentage": 97.09,
            "rank": 42
        }
    },
    {
        "OMRID": 33720635,
        "name": "Mcconnell Miranda",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 105,
            "percentage": 97.39,
            "rank": 82
        },
        "chemistry": {
            "marks": 113,
            "percentage": 99.91,
            "rank": 15
        },
        "maths": {
            "marks": 112,
            "percentage": 97.04,
            "rank": 65
        },
        "total": {
            "marks": 300,
            "percentage": 99.64,
            "rank": 61
        }
    },
    {
        "OMRID": 80688836,
        "name": "Kelsey Oconnor",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 103,
            "percentage": 99.36,
            "rank": 99
        },
        "chemistry": {
            "marks": 106,
            "percentage": 96.38,
            "rank": 32
        },
        "maths": {
            "marks": 106,
            "percentage": 99.02,
            "rank": 67
        },
        "total": {
            "marks": 345,
            "percentage": 96.24,
            "rank": 71
        }
    },
    {
        "OMRID": 54696837,
        "name": "Hodge Banks",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 111,
            "percentage": 96.5,
            "rank": 96
        },
        "chemistry": {
            "marks": 116,
            "percentage": 99.79,
            "rank": 13
        },
        "maths": {
            "marks": 103,
            "percentage": 97.62,
            "rank": 33
        },
        "total": {
            "marks": 335,
            "percentage": 99.75,
            "rank": 80
        }
    },
    {
        "OMRID": 66807138,
        "name": "Kramer Cummings",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 116,
            "percentage": 95.24,
            "rank": 16
        },
        "chemistry": {
            "marks": 105,
            "percentage": 99.62,
            "rank": 61
        },
        "maths": {
            "marks": 104,
            "percentage": 96.89,
            "rank": 51
        },
        "total": {
            "marks": 313,
            "percentage": 98.07,
            "rank": 72
        }
    },
    {
        "OMRID": 79933039,
        "name": "Hickman Irwin",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 103,
            "percentage": 95.28,
            "rank": 40
        },
        "chemistry": {
            "marks": 106,
            "percentage": 96.27,
            "rank": 15
        },
        "maths": {
            "marks": 119,
            "percentage": 97.16,
            "rank": 40
        },
        "total": {
            "marks": 309,
            "percentage": 99.65,
            "rank": 48
        }
    },
    {
        "OMRID": 87725640,
        "name": "Ortiz Sears",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 113,
            "percentage": 96.53,
            "rank": 93
        },
        "chemistry": {
            "marks": 101,
            "percentage": 95.5,
            "rank": 87
        },
        "maths": {
            "marks": 105,
            "percentage": 99.76,
            "rank": 21
        },
        "total": {
            "marks": 348,
            "percentage": 98.36,
            "rank": 94
        }
    },
    {
        "OMRID": 60089441,
        "name": "Schneider Dunlap",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 118,
            "percentage": 97.47,
            "rank": 98
        },
        "chemistry": {
            "marks": 117,
            "percentage": 97.97,
            "rank": 23
        },
        "maths": {
            "marks": 100,
            "percentage": 96.78,
            "rank": 6
        },
        "total": {
            "marks": 338,
            "percentage": 99.58,
            "rank": 97
        }
    },
    {
        "OMRID": 54042742,
        "name": "Linda Emerson",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 109,
            "percentage": 96.9,
            "rank": 96
        },
        "chemistry": {
            "marks": 105,
            "percentage": 98.42,
            "rank": 27
        },
        "maths": {
            "marks": 103,
            "percentage": 99.67,
            "rank": 4
        },
        "total": {
            "marks": 344,
            "percentage": 95.87,
            "rank": 25
        }
    },
    {
        "OMRID": 39112343,
        "name": "Ayala Maldonado",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 110,
            "percentage": 99.22,
            "rank": 20
        },
        "chemistry": {
            "marks": 114,
            "percentage": 96.13,
            "rank": 80
        },
        "maths": {
            "marks": 116,
            "percentage": 96.32,
            "rank": 90
        },
        "total": {
            "marks": 319,
            "percentage": 98.72,
            "rank": 47
        }
    },
    {
        "OMRID": 18792944,
        "name": "Hurst Mcneil",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 101,
            "percentage": 99.66,
            "rank": 37
        },
        "chemistry": {
            "marks": 105,
            "percentage": 99.46,
            "rank": 24
        },
        "maths": {
            "marks": 118,
            "percentage": 98.45,
            "rank": 28
        },
        "total": {
            "marks": 311,
            "percentage": 96.38,
            "rank": 25
        }
    },
    {
        "OMRID": 19067745,
        "name": "Sawyer Donovan",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 109,
            "percentage": 99.47,
            "rank": 35
        },
        "chemistry": {
            "marks": 104,
            "percentage": 99.15,
            "rank": 70
        },
        "maths": {
            "marks": 104,
            "percentage": 97.49,
            "rank": 50
        },
        "total": {
            "marks": 329,
            "percentage": 96.23,
            "rank": 20
        }
    },
    {
        "OMRID": 99558846,
        "name": "Carpenter Evans",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 100,
            "percentage": 99.06,
            "rank": 74
        },
        "chemistry": {
            "marks": 115,
            "percentage": 96.52,
            "rank": 52
        },
        "maths": {
            "marks": 120,
            "percentage": 96.99,
            "rank": 8
        },
        "total": {
            "marks": 358,
            "percentage": 99.25,
            "rank": 88
        }
    },
    {
        "OMRID": 29261347,
        "name": "Chelsea Stein",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 105,
            "percentage": 98.44,
            "rank": 59
        },
        "chemistry": {
            "marks": 109,
            "percentage": 96.7,
            "rank": 77
        },
        "maths": {
            "marks": 111,
            "percentage": 96.35,
            "rank": 92
        },
        "total": {
            "marks": 314,
            "percentage": 96.84,
            "rank": 74
        }
    },
    {
        "OMRID": 39862548,
        "name": "Flynn Cardenas",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 105,
            "percentage": 96.4,
            "rank": 47
        },
        "chemistry": {
            "marks": 100,
            "percentage": 99.94,
            "rank": 90
        },
        "maths": {
            "marks": 108,
            "percentage": 98.88,
            "rank": 68
        },
        "total": {
            "marks": 344,
            "percentage": 99.51,
            "rank": 40
        }
    },
    {
        "OMRID": 39716049,
        "name": "Lessie Mendoza",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 111,
            "percentage": 98.54,
            "rank": 86
        },
        "chemistry": {
            "marks": 105,
            "percentage": 96.97,
            "rank": 91
        },
        "maths": {
            "marks": 112,
            "percentage": 95.69,
            "rank": 18
        },
        "total": {
            "marks": 307,
            "percentage": 99.01,
            "rank": 29
        }
    },
    {
        "OMRID": 68922750,
        "name": "Hammond Cain",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 120,
            "percentage": 97.64,
            "rank": 75
        },
        "chemistry": {
            "marks": 114,
            "percentage": 97.27,
            "rank": 80
        },
        "maths": {
            "marks": 104,
            "percentage": 95.92,
            "rank": 44
        },
        "total": {
            "marks": 302,
            "percentage": 95.94,
            "rank": 60
        }
    },
    {
        "OMRID": 98862151,
        "name": "Reid Bowen",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 102,
            "percentage": 99.97,
            "rank": 22
        },
        "chemistry": {
            "marks": 111,
            "percentage": 99.7,
            "rank": 4
        },
        "maths": {
            "marks": 106,
            "percentage": 97.94,
            "rank": 75
        },
        "total": {
            "marks": 312,
            "percentage": 96.82,
            "rank": 28
        }
    },
    {
        "OMRID": 53200752,
        "name": "Simpson Obrien",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 104,
            "percentage": 95.36,
            "rank": 9
        },
        "chemistry": {
            "marks": 110,
            "percentage": 97.19,
            "rank": 91
        },
        "maths": {
            "marks": 100,
            "percentage": 99.68,
            "rank": 60
        },
        "total": {
            "marks": 340,
            "percentage": 97.32,
            "rank": 32
        }
    },
    {
        "OMRID": 71098353,
        "name": "Lowe Decker",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 108,
            "percentage": 99.29,
            "rank": 20
        },
        "chemistry": {
            "marks": 117,
            "percentage": 96.51,
            "rank": 38
        },
        "maths": {
            "marks": 117,
            "percentage": 95.39,
            "rank": 57
        },
        "total": {
            "marks": 338,
            "percentage": 95.45,
            "rank": 33
        }
    },
    {
        "OMRID": 40424454,
        "name": "Landry Watkins",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 110,
            "percentage": 97,
            "rank": 98
        },
        "chemistry": {
            "marks": 104,
            "percentage": 96.6,
            "rank": 76
        },
        "maths": {
            "marks": 120,
            "percentage": 95.87,
            "rank": 39
        },
        "total": {
            "marks": 357,
            "percentage": 97.41,
            "rank": 95
        }
    },
    {
        "OMRID": 94364255,
        "name": "Cash Madden",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 110,
            "percentage": 95.71,
            "rank": 76
        },
        "chemistry": {
            "marks": 112,
            "percentage": 98.7,
            "rank": 43
        },
        "maths": {
            "marks": 104,
            "percentage": 98.3,
            "rank": 23
        },
        "total": {
            "marks": 340,
            "percentage": 98.15,
            "rank": 80
        }
    },
    {
        "OMRID": 42873456,
        "name": "Stella Savage",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 116,
            "percentage": 95.1,
            "rank": 5
        },
        "chemistry": {
            "marks": 116,
            "percentage": 98.03,
            "rank": 53
        },
        "maths": {
            "marks": 113,
            "percentage": 99.82,
            "rank": 91
        },
        "total": {
            "marks": 353,
            "percentage": 95.04,
            "rank": 12
        }
    },
    {
        "OMRID": 63016657,
        "name": "Potter Beasley",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 110,
            "percentage": 95.28,
            "rank": 64
        },
        "chemistry": {
            "marks": 104,
            "percentage": 96.25,
            "rank": 72
        },
        "maths": {
            "marks": 104,
            "percentage": 97.17,
            "rank": 76
        },
        "total": {
            "marks": 308,
            "percentage": 96.13,
            "rank": 64
        }
    },
    {
        "OMRID": 68130858,
        "name": "Drake Nash",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 101,
            "percentage": 98.78,
            "rank": 62
        },
        "chemistry": {
            "marks": 118,
            "percentage": 98.15,
            "rank": 9
        },
        "maths": {
            "marks": 112,
            "percentage": 96.89,
            "rank": 65
        },
        "total": {
            "marks": 356,
            "percentage": 98.56,
            "rank": 87
        }
    },
    {
        "OMRID": 83051359,
        "name": "Carney Bennett",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 115,
            "percentage": 95.05,
            "rank": 49
        },
        "chemistry": {
            "marks": 116,
            "percentage": 99.19,
            "rank": 75
        },
        "maths": {
            "marks": 110,
            "percentage": 98.6,
            "rank": 55
        },
        "total": {
            "marks": 358,
            "percentage": 96.05,
            "rank": 44
        }
    },
    {
        "OMRID": 53183760,
        "name": "Howell Hampton",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 108,
            "percentage": 96.02,
            "rank": 100
        },
        "chemistry": {
            "marks": 117,
            "percentage": 96.78,
            "rank": 82
        },
        "maths": {
            "marks": 115,
            "percentage": 97.52,
            "rank": 13
        },
        "total": {
            "marks": 325,
            "percentage": 97.34,
            "rank": 3
        }
    },
    {
        "OMRID": 47783361,
        "name": "Lynette Burch",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 120,
            "percentage": 95.27,
            "rank": 66
        },
        "chemistry": {
            "marks": 110,
            "percentage": 97.79,
            "rank": 94
        },
        "maths": {
            "marks": 101,
            "percentage": 96.34,
            "rank": 3
        },
        "total": {
            "marks": 338,
            "percentage": 95.34,
            "rank": 79
        }
    },
    {
        "OMRID": 57913462,
        "name": "Walsh Herring",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 115,
            "percentage": 99.88,
            "rank": 4
        },
        "chemistry": {
            "marks": 113,
            "percentage": 95.3,
            "rank": 24
        },
        "maths": {
            "marks": 111,
            "percentage": 96.89,
            "rank": 1
        },
        "total": {
            "marks": 360,
            "percentage": 99.02,
            "rank": 77
        }
    },
    {
        "OMRID": 17547963,
        "name": "Faith Black",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 106,
            "percentage": 96.63,
            "rank": 41
        },
        "chemistry": {
            "marks": 106,
            "percentage": 98.14,
            "rank": 95
        },
        "maths": {
            "marks": 100,
            "percentage": 99.09,
            "rank": 73
        },
        "total": {
            "marks": 341,
            "percentage": 99.68,
            "rank": 83
        }
    },
    {
        "OMRID": 73737464,
        "name": "Sandy Stanton",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 117,
            "percentage": 99.23,
            "rank": 30
        },
        "chemistry": {
            "marks": 120,
            "percentage": 98.41,
            "rank": 41
        },
        "maths": {
            "marks": 118,
            "percentage": 99.02,
            "rank": 35
        },
        "total": {
            "marks": 300,
            "percentage": 95.11,
            "rank": 27
        }
    },
    {
        "OMRID": 17344765,
        "name": "Mullen Melendez",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 104,
            "percentage": 99.66,
            "rank": 41
        },
        "chemistry": {
            "marks": 120,
            "percentage": 95.34,
            "rank": 73
        },
        "maths": {
            "marks": 102,
            "percentage": 98.18,
            "rank": 92
        },
        "total": {
            "marks": 337,
            "percentage": 98.18,
            "rank": 88
        }
    },
    {
        "OMRID": 50100266,
        "name": "Elma Beach",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 116,
            "percentage": 98.53,
            "rank": 41
        },
        "chemistry": {
            "marks": 113,
            "percentage": 97.07,
            "rank": 26
        },
        "maths": {
            "marks": 105,
            "percentage": 98.16,
            "rank": 69
        },
        "total": {
            "marks": 344,
            "percentage": 98.9,
            "rank": 29
        }
    },
    {
        "OMRID": 14179267,
        "name": "Keisha Pearson",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 114,
            "percentage": 98.47,
            "rank": 77
        },
        "chemistry": {
            "marks": 107,
            "percentage": 95.15,
            "rank": 90
        },
        "maths": {
            "marks": 100,
            "percentage": 97.8,
            "rank": 22
        },
        "total": {
            "marks": 358,
            "percentage": 99.65,
            "rank": 62
        }
    },
    {
        "OMRID": 76154668,
        "name": "Delgado Cohen",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 104,
            "percentage": 97.61,
            "rank": 64
        },
        "chemistry": {
            "marks": 113,
            "percentage": 95.17,
            "rank": 48
        },
        "maths": {
            "marks": 116,
            "percentage": 96.94,
            "rank": 72
        },
        "total": {
            "marks": 311,
            "percentage": 95.26,
            "rank": 3
        }
    },
    {
        "OMRID": 55250769,
        "name": "Vera Bauer",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 119,
            "percentage": 96.08,
            "rank": 3
        },
        "chemistry": {
            "marks": 118,
            "percentage": 98.45,
            "rank": 7
        },
        "maths": {
            "marks": 113,
            "percentage": 97.31,
            "rank": 11
        },
        "total": {
            "marks": 302,
            "percentage": 97.51,
            "rank": 26
        }
    },
    {
        "OMRID": 39103270,
        "name": "Osborne Lester",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 107,
            "percentage": 98.03,
            "rank": 60
        },
        "chemistry": {
            "marks": 120,
            "percentage": 96.62,
            "rank": 23
        },
        "maths": {
            "marks": 101,
            "percentage": 96.34,
            "rank": 14
        },
        "total": {
            "marks": 351,
            "percentage": 99.29,
            "rank": 78
        }
    },
    {
        "OMRID": 67878071,
        "name": "Crosby Workman",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 102,
            "percentage": 95.82,
            "rank": 5
        },
        "chemistry": {
            "marks": 106,
            "percentage": 99.8,
            "rank": 37
        },
        "maths": {
            "marks": 105,
            "percentage": 95.6,
            "rank": 76
        },
        "total": {
            "marks": 360,
            "percentage": 97.83,
            "rank": 75
        }
    },
    {
        "OMRID": 25561572,
        "name": "Louise Mayer",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 103,
            "percentage": 99.31,
            "rank": 72
        },
        "chemistry": {
            "marks": 104,
            "percentage": 96.58,
            "rank": 16
        },
        "maths": {
            "marks": 105,
            "percentage": 96.07,
            "rank": 29
        },
        "total": {
            "marks": 308,
            "percentage": 96.6,
            "rank": 8
        }
    },
    {
        "OMRID": 12149573,
        "name": "Skinner Hayes",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 109,
            "percentage": 95.12,
            "rank": 44
        },
        "chemistry": {
            "marks": 111,
            "percentage": 98.85,
            "rank": 10
        },
        "maths": {
            "marks": 120,
            "percentage": 98.17,
            "rank": 49
        },
        "total": {
            "marks": 342,
            "percentage": 97.52,
            "rank": 20
        }
    },
    {
        "OMRID": 93069874,
        "name": "Reynolds Marshall",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 118,
            "percentage": 98.87,
            "rank": 2
        },
        "chemistry": {
            "marks": 101,
            "percentage": 96.14,
            "rank": 40
        },
        "maths": {
            "marks": 114,
            "percentage": 97.47,
            "rank": 10
        },
        "total": {
            "marks": 351,
            "percentage": 97.49,
            "rank": 22
        }
    },
    {
        "OMRID": 31139675,
        "name": "Castro Ware",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 112,
            "percentage": 99.95,
            "rank": 80
        },
        "chemistry": {
            "marks": 107,
            "percentage": 99.32,
            "rank": 19
        },
        "maths": {
            "marks": 102,
            "percentage": 98.19,
            "rank": 22
        },
        "total": {
            "marks": 302,
            "percentage": 99.92,
            "rank": 22
        }
    },
    {
        "OMRID": 36504276,
        "name": "Gould Davis",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 101,
            "percentage": 96.66,
            "rank": 4
        },
        "chemistry": {
            "marks": 107,
            "percentage": 96.49,
            "rank": 93
        },
        "maths": {
            "marks": 102,
            "percentage": 96.7,
            "rank": 30
        },
        "total": {
            "marks": 311,
            "percentage": 97.21,
            "rank": 42
        }
    },
    {
        "OMRID": 69164777,
        "name": "Alicia Cameron",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 120,
            "percentage": 97.18,
            "rank": 65
        },
        "chemistry": {
            "marks": 111,
            "percentage": 95.94,
            "rank": 74
        },
        "maths": {
            "marks": 120,
            "percentage": 98.39,
            "rank": 88
        },
        "total": {
            "marks": 340,
            "percentage": 98.61,
            "rank": 74
        }
    },
    {
        "OMRID": 22304078,
        "name": "Rodriguez Robbins",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 104,
            "percentage": 99.76,
            "rank": 62
        },
        "chemistry": {
            "marks": 120,
            "percentage": 97.71,
            "rank": 2
        },
        "maths": {
            "marks": 103,
            "percentage": 95.86,
            "rank": 71
        },
        "total": {
            "marks": 305,
            "percentage": 99.53,
            "rank": 59
        }
    },
    {
        "OMRID": 14208179,
        "name": "Melanie Petty",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 105,
            "percentage": 97.49,
            "rank": 39
        },
        "chemistry": {
            "marks": 101,
            "percentage": 98,
            "rank": 89
        },
        "maths": {
            "marks": 110,
            "percentage": 97.47,
            "rank": 56
        },
        "total": {
            "marks": 329,
            "percentage": 98.02,
            "rank": 63
        }
    },
    {
        "OMRID": 99111180,
        "name": "Desiree Cline",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 111,
            "percentage": 97.63,
            "rank": 6
        },
        "chemistry": {
            "marks": 113,
            "percentage": 96.62,
            "rank": 57
        },
        "maths": {
            "marks": 117,
            "percentage": 98.17,
            "rank": 9
        },
        "total": {
            "marks": 314,
            "percentage": 96.41,
            "rank": 62
        }
    },
    {
        "OMRID": 34697481,
        "name": "Cain Gilmore",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 110,
            "percentage": 99.58,
            "rank": 56
        },
        "chemistry": {
            "marks": 108,
            "percentage": 99.37,
            "rank": 79
        },
        "maths": {
            "marks": 116,
            "percentage": 98.36,
            "rank": 30
        },
        "total": {
            "marks": 348,
            "percentage": 95.16,
            "rank": 42
        }
    },
    {
        "OMRID": 60833182,
        "name": "Bradley Potts",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 104,
            "percentage": 97.78,
            "rank": 57
        },
        "chemistry": {
            "marks": 119,
            "percentage": 97.8,
            "rank": 28
        },
        "maths": {
            "marks": 113,
            "percentage": 99.2,
            "rank": 82
        },
        "total": {
            "marks": 337,
            "percentage": 99.09,
            "rank": 80
        }
    },
    {
        "OMRID": 68165083,
        "name": "Annabelle Becker",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 118,
            "percentage": 95.69,
            "rank": 59
        },
        "chemistry": {
            "marks": 110,
            "percentage": 99.29,
            "rank": 37
        },
        "maths": {
            "marks": 103,
            "percentage": 98.16,
            "rank": 33
        },
        "total": {
            "marks": 308,
            "percentage": 96.84,
            "rank": 14
        }
    },
    {
        "OMRID": 33501884,
        "name": "Finch Small",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 101,
            "percentage": 95.05,
            "rank": 54
        },
        "chemistry": {
            "marks": 114,
            "percentage": 98.94,
            "rank": 46
        },
        "maths": {
            "marks": 109,
            "percentage": 97.29,
            "rank": 37
        },
        "total": {
            "marks": 333,
            "percentage": 98.13,
            "rank": 95
        }
    },
    {
        "OMRID": 39020885,
        "name": "Amie Underwood",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 104,
            "percentage": 95.32,
            "rank": 64
        },
        "chemistry": {
            "marks": 106,
            "percentage": 97.71,
            "rank": 56
        },
        "maths": {
            "marks": 102,
            "percentage": 95.72,
            "rank": 27
        },
        "total": {
            "marks": 359,
            "percentage": 96.99,
            "rank": 93
        }
    },
    {
        "OMRID": 79363986,
        "name": "Shana Marquez",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 119,
            "percentage": 96.66,
            "rank": 26
        },
        "chemistry": {
            "marks": 104,
            "percentage": 99.27,
            "rank": 69
        },
        "maths": {
            "marks": 120,
            "percentage": 97.56,
            "rank": 90
        },
        "total": {
            "marks": 327,
            "percentage": 97.27,
            "rank": 44
        }
    },
    {
        "OMRID": 17242487,
        "name": "Waller Hoffman",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 108,
            "percentage": 99.62,
            "rank": 51
        },
        "chemistry": {
            "marks": 102,
            "percentage": 95.63,
            "rank": 36
        },
        "maths": {
            "marks": 114,
            "percentage": 98.49,
            "rank": 87
        },
        "total": {
            "marks": 335,
            "percentage": 98.97,
            "rank": 19
        }
    },
    {
        "OMRID": 94492188,
        "name": "Eleanor Richmond",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 112,
            "percentage": 99.53,
            "rank": 22
        },
        "chemistry": {
            "marks": 116,
            "percentage": 98.32,
            "rank": 82
        },
        "maths": {
            "marks": 119,
            "percentage": 97.86,
            "rank": 95
        },
        "total": {
            "marks": 315,
            "percentage": 98.63,
            "rank": 3
        }
    },
    {
        "OMRID": 76167889,
        "name": "Porter Pennington",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 120,
            "percentage": 97.44,
            "rank": 47
        },
        "chemistry": {
            "marks": 116,
            "percentage": 97.44,
            "rank": 51
        },
        "maths": {
            "marks": 102,
            "percentage": 99.25,
            "rank": 57
        },
        "total": {
            "marks": 318,
            "percentage": 97.82,
            "rank": 10
        }
    },
    {
        "OMRID": 97591290,
        "name": "Gonzales Barton",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 111,
            "percentage": 98.33,
            "rank": 20
        },
        "chemistry": {
            "marks": 114,
            "percentage": 97.94,
            "rank": 14
        },
        "maths": {
            "marks": 101,
            "percentage": 99.23,
            "rank": 31
        },
        "total": {
            "marks": 360,
            "percentage": 95.24,
            "rank": 22
        }
    },
    {
        "OMRID": 20770991,
        "name": "Serena Vang",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 106,
            "percentage": 95.94,
            "rank": 100
        },
        "chemistry": {
            "marks": 117,
            "percentage": 97.79,
            "rank": 14
        },
        "maths": {
            "marks": 120,
            "percentage": 95.66,
            "rank": 51
        },
        "total": {
            "marks": 338,
            "percentage": 95.49,
            "rank": 97
        }
    },
    {
        "OMRID": 60975492,
        "name": "Glenn Allison",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 117,
            "percentage": 97.07,
            "rank": 58
        },
        "chemistry": {
            "marks": 112,
            "percentage": 97.44,
            "rank": 51
        },
        "maths": {
            "marks": 106,
            "percentage": 96.91,
            "rank": 83
        },
        "total": {
            "marks": 323,
            "percentage": 98.27,
            "rank": 10
        }
    },
    {
        "OMRID": 61980593,
        "name": "Katina Spears",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 105,
            "percentage": 97.62,
            "rank": 85
        },
        "chemistry": {
            "marks": 117,
            "percentage": 96.45,
            "rank": 61
        },
        "maths": {
            "marks": 100,
            "percentage": 98.41,
            "rank": 57
        },
        "total": {
            "marks": 318,
            "percentage": 98.76,
            "rank": 75
        }
    },
    {
        "OMRID": 36129094,
        "name": "Wanda Edwards",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 108,
            "percentage": 95.1,
            "rank": 2
        },
        "chemistry": {
            "marks": 113,
            "percentage": 97.85,
            "rank": 91
        },
        "maths": {
            "marks": 100,
            "percentage": 99.52,
            "rank": 98
        },
        "total": {
            "marks": 342,
            "percentage": 95.42,
            "rank": 89
        }
    },
    {
        "OMRID": 82293895,
        "name": "Georgia Lynn",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 118,
            "percentage": 95.33,
            "rank": 82
        },
        "chemistry": {
            "marks": 109,
            "percentage": 98.48,
            "rank": 95
        },
        "maths": {
            "marks": 109,
            "percentage": 98.3,
            "rank": 55
        },
        "total": {
            "marks": 341,
            "percentage": 96.87,
            "rank": 35
        }
    },
    {
        "OMRID": 76685496,
        "name": "Lucile Juarez",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 108,
            "percentage": 98.26,
            "rank": 99
        },
        "chemistry": {
            "marks": 108,
            "percentage": 98.44,
            "rank": 71
        },
        "maths": {
            "marks": 108,
            "percentage": 97.81,
            "rank": 79
        },
        "total": {
            "marks": 337,
            "percentage": 95.67,
            "rank": 68
        }
    },
    {
        "OMRID": 13090897,
        "name": "Elvira Moss",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 110,
            "percentage": 96.36,
            "rank": 93
        },
        "chemistry": {
            "marks": 106,
            "percentage": 98.01,
            "rank": 40
        },
        "maths": {
            "marks": 109,
            "percentage": 97.91,
            "rank": 87
        },
        "total": {
            "marks": 322,
            "percentage": 98.31,
            "rank": 44
        }
    },
    {
        "OMRID": 25827598,
        "name": "Emma Harding",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 113,
            "percentage": 97.64,
            "rank": 94
        },
        "chemistry": {
            "marks": 106,
            "percentage": 99.12,
            "rank": 88
        },
        "maths": {
            "marks": 107,
            "percentage": 96.89,
            "rank": 40
        },
        "total": {
            "marks": 353,
            "percentage": 96.93,
            "rank": 64
        }
    },
    {
        "OMRID": 54154299,
        "name": "Morse Church",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 120,
            "percentage": 95.57,
            "rank": 5
        },
        "chemistry": {
            "marks": 101,
            "percentage": 98.26,
            "rank": 33
        },
        "maths": {
            "marks": 105,
            "percentage": 97.38,
            "rank": 70
        },
        "total": {
            "marks": 353,
            "percentage": 95.6,
            "rank": 45
        }
    },
    {
        "OMRID": 590730100,
        "name": "Brandie Delaney",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 119,
            "percentage": 99.58,
            "rank": 97
        },
        "chemistry": {
            "marks": 111,
            "percentage": 97.52,
            "rank": 26
        },
        "maths": {
            "marks": 115,
            "percentage": 98.1,
            "rank": 7
        },
        "total": {
            "marks": 333,
            "percentage": 96.14,
            "rank": 47
        }
    },
    {
        "OMRID": 103653101,
        "name": "Valerie Hays",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 107,
            "percentage": 96.81,
            "rank": 13
        },
        "chemistry": {
            "marks": 103,
            "percentage": 95.53,
            "rank": 33
        },
        "maths": {
            "marks": 109,
            "percentage": 98.37,
            "rank": 42
        },
        "total": {
            "marks": 341,
            "percentage": 96.98,
            "rank": 24
        }
    },
    {
        "OMRID": 214282102,
        "name": "Crystal Gillespie",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 102,
            "percentage": 96.09,
            "rank": 18
        },
        "chemistry": {
            "marks": 112,
            "percentage": 95.5,
            "rank": 22
        },
        "maths": {
            "marks": 103,
            "percentage": 95.8,
            "rank": 31
        },
        "total": {
            "marks": 346,
            "percentage": 96.97,
            "rank": 56
        }
    },
    {
        "OMRID": 142443103,
        "name": "Case Mccall",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 100,
            "percentage": 98.27,
            "rank": 49
        },
        "chemistry": {
            "marks": 107,
            "percentage": 96.05,
            "rank": 16
        },
        "maths": {
            "marks": 102,
            "percentage": 95.68,
            "rank": 37
        },
        "total": {
            "marks": 360,
            "percentage": 95.38,
            "rank": 9
        }
    },
    {
        "OMRID": 642306104,
        "name": "Rosanne Ortiz",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 113,
            "percentage": 96.61,
            "rank": 23
        },
        "chemistry": {
            "marks": 113,
            "percentage": 96.77,
            "rank": 94
        },
        "maths": {
            "marks": 109,
            "percentage": 98.42,
            "rank": 95
        },
        "total": {
            "marks": 330,
            "percentage": 95.49,
            "rank": 81
        }
    },
    {
        "OMRID": 836777105,
        "name": "Nelson Mcleod",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 104,
            "percentage": 96.39,
            "rank": 59
        },
        "chemistry": {
            "marks": 108,
            "percentage": 96.03,
            "rank": 74
        },
        "maths": {
            "marks": 102,
            "percentage": 98.24,
            "rank": 58
        },
        "total": {
            "marks": 312,
            "percentage": 99.77,
            "rank": 5
        }
    },
    {
        "OMRID": 935162106,
        "name": "Cheri Garrison",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 120,
            "percentage": 95.23,
            "rank": 93
        },
        "chemistry": {
            "marks": 100,
            "percentage": 96.36,
            "rank": 74
        },
        "maths": {
            "marks": 109,
            "percentage": 96.9,
            "rank": 22
        },
        "total": {
            "marks": 313,
            "percentage": 95.12,
            "rank": 23
        }
    },
    {
        "OMRID": 642750107,
        "name": "Larson Watts",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 101,
            "percentage": 97.9,
            "rank": 65
        },
        "chemistry": {
            "marks": 110,
            "percentage": 98.58,
            "rank": 61
        },
        "maths": {
            "marks": 106,
            "percentage": 98.69,
            "rank": 1
        },
        "total": {
            "marks": 359,
            "percentage": 96.9,
            "rank": 78
        }
    },
    {
        "OMRID": 609877108,
        "name": "Isabella Walters",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 110,
            "percentage": 99.69,
            "rank": 50
        },
        "chemistry": {
            "marks": 106,
            "percentage": 96.17,
            "rank": 33
        },
        "maths": {
            "marks": 107,
            "percentage": 98.12,
            "rank": 94
        },
        "total": {
            "marks": 304,
            "percentage": 95.9,
            "rank": 52
        }
    },
    {
        "OMRID": 425618109,
        "name": "Wyatt Talley",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 102,
            "percentage": 99.43,
            "rank": 21
        },
        "chemistry": {
            "marks": 107,
            "percentage": 98.64,
            "rank": 12
        },
        "maths": {
            "marks": 109,
            "percentage": 97.88,
            "rank": 85
        },
        "total": {
            "marks": 344,
            "percentage": 95.51,
            "rank": 63
        }
    },
    {
        "OMRID": 191965110,
        "name": "Randall Ryan",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 107,
            "percentage": 98.8,
            "rank": 74
        },
        "chemistry": {
            "marks": 117,
            "percentage": 99.06,
            "rank": 39
        },
        "maths": {
            "marks": 113,
            "percentage": 97.33,
            "rank": 92
        },
        "total": {
            "marks": 308,
            "percentage": 99.08,
            "rank": 4
        }
    },
    {
        "OMRID": 935673111,
        "name": "Susie Nelson",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 102,
            "percentage": 99.89,
            "rank": 88
        },
        "chemistry": {
            "marks": 110,
            "percentage": 99.67,
            "rank": 6
        },
        "maths": {
            "marks": 102,
            "percentage": 96.17,
            "rank": 32
        },
        "total": {
            "marks": 330,
            "percentage": 99.92,
            "rank": 8
        }
    },
    {
        "OMRID": 737117112,
        "name": "Jennifer Buckner",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 110,
            "percentage": 96.57,
            "rank": 87
        },
        "chemistry": {
            "marks": 113,
            "percentage": 95.99,
            "rank": 79
        },
        "maths": {
            "marks": 119,
            "percentage": 95.45,
            "rank": 83
        },
        "total": {
            "marks": 306,
            "percentage": 98.23,
            "rank": 100
        }
    },
    {
        "OMRID": 960293113,
        "name": "Ola Jennings",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 107,
            "percentage": 97.98,
            "rank": 45
        },
        "chemistry": {
            "marks": 110,
            "percentage": 98.47,
            "rank": 3
        },
        "maths": {
            "marks": 101,
            "percentage": 95.97,
            "rank": 60
        },
        "total": {
            "marks": 358,
            "percentage": 95.75,
            "rank": 1
        }
    },
    {
        "OMRID": 360611114,
        "name": "Lucille Wynn",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 109,
            "percentage": 98.31,
            "rank": 7
        },
        "chemistry": {
            "marks": 104,
            "percentage": 96.23,
            "rank": 38
        },
        "maths": {
            "marks": 109,
            "percentage": 98.76,
            "rank": 57
        },
        "total": {
            "marks": 333,
            "percentage": 98.55,
            "rank": 97
        }
    },
    {
        "OMRID": 666280115,
        "name": "Tammie Brooks",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 119,
            "percentage": 97.27,
            "rank": 75
        },
        "chemistry": {
            "marks": 105,
            "percentage": 95.68,
            "rank": 72
        },
        "maths": {
            "marks": 109,
            "percentage": 97.79,
            "rank": 35
        },
        "total": {
            "marks": 318,
            "percentage": 95.01,
            "rank": 73
        }
    },
    {
        "OMRID": 874568116,
        "name": "Imogene Dale",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 106,
            "percentage": 96.3,
            "rank": 16
        },
        "chemistry": {
            "marks": 107,
            "percentage": 98.21,
            "rank": 94
        },
        "maths": {
            "marks": 106,
            "percentage": 99.68,
            "rank": 32
        },
        "total": {
            "marks": 301,
            "percentage": 95.87,
            "rank": 82
        }
    },
    {
        "OMRID": 811109117,
        "name": "Georgette Lott",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 109,
            "percentage": 98.26,
            "rank": 77
        },
        "chemistry": {
            "marks": 107,
            "percentage": 97.32,
            "rank": 96
        },
        "maths": {
            "marks": 116,
            "percentage": 98.81,
            "rank": 50
        },
        "total": {
            "marks": 309,
            "percentage": 96.66,
            "rank": 39
        }
    },
    {
        "OMRID": 914210118,
        "name": "Bryan Morse",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 116,
            "percentage": 95.38,
            "rank": 12
        },
        "chemistry": {
            "marks": 103,
            "percentage": 95.7,
            "rank": 7
        },
        "maths": {
            "marks": 100,
            "percentage": 99.76,
            "rank": 94
        },
        "total": {
            "marks": 322,
            "percentage": 98.46,
            "rank": 46
        }
    },
    {
        "OMRID": 711781119,
        "name": "Holland Frank",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 100,
            "percentage": 98.47,
            "rank": 85
        },
        "chemistry": {
            "marks": 120,
            "percentage": 96.6,
            "rank": 79
        },
        "maths": {
            "marks": 110,
            "percentage": 98.43,
            "rank": 5
        },
        "total": {
            "marks": 344,
            "percentage": 99.3,
            "rank": 97
        }
    },
    {
        "OMRID": 557465120,
        "name": "Vaughan Mcdonald",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 103,
            "percentage": 99.89,
            "rank": 41
        },
        "chemistry": {
            "marks": 108,
            "percentage": 96.7,
            "rank": 97
        },
        "maths": {
            "marks": 102,
            "percentage": 95.21,
            "rank": 70
        },
        "total": {
            "marks": 301,
            "percentage": 96.77,
            "rank": 14
        }
    },
    {
        "OMRID": 869610121,
        "name": "Nixon Rivers",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 106,
            "percentage": 99.67,
            "rank": 67
        },
        "chemistry": {
            "marks": 107,
            "percentage": 98.83,
            "rank": 25
        },
        "maths": {
            "marks": 100,
            "percentage": 96.4,
            "rank": 54
        },
        "total": {
            "marks": 350,
            "percentage": 99.1,
            "rank": 92
        }
    },
    {
        "OMRID": 732335122,
        "name": "Norris Meyers",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 117,
            "percentage": 96.04,
            "rank": 82
        },
        "chemistry": {
            "marks": 106,
            "percentage": 96.78,
            "rank": 96
        },
        "maths": {
            "marks": 120,
            "percentage": 96.72,
            "rank": 39
        },
        "total": {
            "marks": 352,
            "percentage": 97.55,
            "rank": 7
        }
    },
    {
        "OMRID": 606660123,
        "name": "Mcguire Nixon",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 115,
            "percentage": 98.69,
            "rank": 45
        },
        "chemistry": {
            "marks": 105,
            "percentage": 96.31,
            "rank": 29
        },
        "maths": {
            "marks": 102,
            "percentage": 96.01,
            "rank": 55
        },
        "total": {
            "marks": 331,
            "percentage": 98.12,
            "rank": 58
        }
    },
    {
        "OMRID": 155747124,
        "name": "Sheri Fisher",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 110,
            "percentage": 98.42,
            "rank": 99
        },
        "chemistry": {
            "marks": 114,
            "percentage": 97.34,
            "rank": 69
        },
        "maths": {
            "marks": 108,
            "percentage": 98.79,
            "rank": 46
        },
        "total": {
            "marks": 303,
            "percentage": 97.07,
            "rank": 28
        }
    },
    {
        "OMRID": 613693125,
        "name": "Samantha Vinson",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 113,
            "percentage": 96.98,
            "rank": 77
        },
        "chemistry": {
            "marks": 116,
            "percentage": 96.28,
            "rank": 84
        },
        "maths": {
            "marks": 106,
            "percentage": 97.22,
            "rank": 99
        },
        "total": {
            "marks": 352,
            "percentage": 95.27,
            "rank": 57
        }
    },
    {
        "OMRID": 679973126,
        "name": "Christa Carson",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 103,
            "percentage": 99.87,
            "rank": 33
        },
        "chemistry": {
            "marks": 106,
            "percentage": 95.39,
            "rank": 90
        },
        "maths": {
            "marks": 108,
            "percentage": 99.26,
            "rank": 5
        },
        "total": {
            "marks": 347,
            "percentage": 97.52,
            "rank": 72
        }
    },
    {
        "OMRID": 347734127,
        "name": "Monique Schroeder",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 115,
            "percentage": 95.18,
            "rank": 80
        },
        "chemistry": {
            "marks": 111,
            "percentage": 95.25,
            "rank": 58
        },
        "maths": {
            "marks": 120,
            "percentage": 98.27,
            "rank": 58
        },
        "total": {
            "marks": 303,
            "percentage": 99.5,
            "rank": 17
        }
    },
    {
        "OMRID": 719079128,
        "name": "Alejandra Price",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 102,
            "percentage": 97.22,
            "rank": 87
        },
        "chemistry": {
            "marks": 113,
            "percentage": 95.13,
            "rank": 22
        },
        "maths": {
            "marks": 108,
            "percentage": 95.62,
            "rank": 77
        },
        "total": {
            "marks": 332,
            "percentage": 95.89,
            "rank": 33
        }
    },
    {
        "OMRID": 416186129,
        "name": "Craft Ewing",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 117,
            "percentage": 97.76,
            "rank": 30
        },
        "chemistry": {
            "marks": 100,
            "percentage": 96.6,
            "rank": 42
        },
        "maths": {
            "marks": 117,
            "percentage": 96.43,
            "rank": 41
        },
        "total": {
            "marks": 332,
            "percentage": 95.11,
            "rank": 53
        }
    },
    {
        "OMRID": 708891130,
        "name": "Clara Merrill",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 112,
            "percentage": 98.33,
            "rank": 47
        },
        "chemistry": {
            "marks": 117,
            "percentage": 99.89,
            "rank": 52
        },
        "maths": {
            "marks": 108,
            "percentage": 99.23,
            "rank": 95
        },
        "total": {
            "marks": 332,
            "percentage": 98.42,
            "rank": 17
        }
    },
    {
        "OMRID": 390083131,
        "name": "Lakeisha Horne",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 119,
            "percentage": 99.95,
            "rank": 26
        },
        "chemistry": {
            "marks": 113,
            "percentage": 99.53,
            "rank": 2
        },
        "maths": {
            "marks": 100,
            "percentage": 98.25,
            "rank": 30
        },
        "total": {
            "marks": 300,
            "percentage": 99.69,
            "rank": 32
        }
    },
    {
        "OMRID": 424226132,
        "name": "Kay Gates",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 120,
            "percentage": 96.37,
            "rank": 94
        },
        "chemistry": {
            "marks": 118,
            "percentage": 99.79,
            "rank": 58
        },
        "maths": {
            "marks": 100,
            "percentage": 96.68,
            "rank": 5
        },
        "total": {
            "marks": 322,
            "percentage": 95.21,
            "rank": 83
        }
    },
    {
        "OMRID": 338626133,
        "name": "Dorothy Leon",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 115,
            "percentage": 95.29,
            "rank": 64
        },
        "chemistry": {
            "marks": 111,
            "percentage": 98.32,
            "rank": 98
        },
        "maths": {
            "marks": 104,
            "percentage": 98.78,
            "rank": 99
        },
        "total": {
            "marks": 300,
            "percentage": 96.86,
            "rank": 61
        }
    },
    {
        "OMRID": 724098134,
        "name": "Pearl Hickman",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 100,
            "percentage": 96.93,
            "rank": 41
        },
        "chemistry": {
            "marks": 116,
            "percentage": 95.99,
            "rank": 19
        },
        "maths": {
            "marks": 100,
            "percentage": 99.1,
            "rank": 20
        },
        "total": {
            "marks": 332,
            "percentage": 96.76,
            "rank": 38
        }
    },
    {
        "OMRID": 896158135,
        "name": "Haley Arnold",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 106,
            "percentage": 95.46,
            "rank": 29
        },
        "chemistry": {
            "marks": 105,
            "percentage": 96.58,
            "rank": 30
        },
        "maths": {
            "marks": 107,
            "percentage": 98.43,
            "rank": 16
        },
        "total": {
            "marks": 329,
            "percentage": 97.54,
            "rank": 55
        }
    },
    {
        "OMRID": 560641136,
        "name": "Cassandra Ratliff",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 115,
            "percentage": 97.57,
            "rank": 89
        },
        "chemistry": {
            "marks": 104,
            "percentage": 97.77,
            "rank": 99
        },
        "maths": {
            "marks": 114,
            "percentage": 95.07,
            "rank": 57
        },
        "total": {
            "marks": 326,
            "percentage": 96.61,
            "rank": 54
        }
    },
    {
        "OMRID": 653622137,
        "name": "Glenda Mercado",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 108,
            "percentage": 96.4,
            "rank": 70
        },
        "chemistry": {
            "marks": 113,
            "percentage": 98.29,
            "rank": 90
        },
        "maths": {
            "marks": 113,
            "percentage": 98.63,
            "rank": 56
        },
        "total": {
            "marks": 333,
            "percentage": 99.71,
            "rank": 64
        }
    },
    {
        "OMRID": 845048138,
        "name": "Dickson Skinner",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 120,
            "percentage": 95.79,
            "rank": 95
        },
        "chemistry": {
            "marks": 112,
            "percentage": 98.51,
            "rank": 30
        },
        "maths": {
            "marks": 101,
            "percentage": 98.33,
            "rank": 99
        },
        "total": {
            "marks": 333,
            "percentage": 98.7,
            "rank": 16
        }
    },
    {
        "OMRID": 938116139,
        "name": "Eddie Maxwell",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 100,
            "percentage": 97.61,
            "rank": 95
        },
        "chemistry": {
            "marks": 116,
            "percentage": 98.82,
            "rank": 58
        },
        "maths": {
            "marks": 105,
            "percentage": 97.93,
            "rank": 42
        },
        "total": {
            "marks": 315,
            "percentage": 96.4,
            "rank": 23
        }
    },
    {
        "OMRID": 425868140,
        "name": "Hanson Potter",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 108,
            "percentage": 99.5,
            "rank": 75
        },
        "chemistry": {
            "marks": 116,
            "percentage": 99.01,
            "rank": 22
        },
        "maths": {
            "marks": 108,
            "percentage": 98.04,
            "rank": 42
        },
        "total": {
            "marks": 347,
            "percentage": 97.34,
            "rank": 88
        }
    },
    {
        "OMRID": 736446141,
        "name": "Sherrie Alvarez",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 113,
            "percentage": 99.01,
            "rank": 67
        },
        "chemistry": {
            "marks": 116,
            "percentage": 99.64,
            "rank": 79
        },
        "maths": {
            "marks": 110,
            "percentage": 99.62,
            "rank": 1
        },
        "total": {
            "marks": 355,
            "percentage": 98.02,
            "rank": 1
        }
    },
    {
        "OMRID": 159523142,
        "name": "Gilliam Fletcher",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 107,
            "percentage": 95.63,
            "rank": 8
        },
        "chemistry": {
            "marks": 102,
            "percentage": 96.97,
            "rank": 59
        },
        "maths": {
            "marks": 104,
            "percentage": 95.64,
            "rank": 8
        },
        "total": {
            "marks": 303,
            "percentage": 96.88,
            "rank": 54
        }
    },
    {
        "OMRID": 515746143,
        "name": "Victoria Carlson",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 104,
            "percentage": 95.16,
            "rank": 21
        },
        "chemistry": {
            "marks": 116,
            "percentage": 96.81,
            "rank": 91
        },
        "maths": {
            "marks": 104,
            "percentage": 98.39,
            "rank": 75
        },
        "total": {
            "marks": 323,
            "percentage": 96.36,
            "rank": 80
        }
    },
    {
        "OMRID": 211636144,
        "name": "Julianne Bartlett",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 102,
            "percentage": 96.83,
            "rank": 94
        },
        "chemistry": {
            "marks": 101,
            "percentage": 98.62,
            "rank": 15
        },
        "maths": {
            "marks": 107,
            "percentage": 98.61,
            "rank": 21
        },
        "total": {
            "marks": 353,
            "percentage": 95.18,
            "rank": 58
        }
    },
    {
        "OMRID": 182862145,
        "name": "Graham Mitchell",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 115,
            "percentage": 98.4,
            "rank": 93
        },
        "chemistry": {
            "marks": 101,
            "percentage": 97.15,
            "rank": 5
        },
        "maths": {
            "marks": 118,
            "percentage": 99.82,
            "rank": 2
        },
        "total": {
            "marks": 338,
            "percentage": 96.83,
            "rank": 72
        }
    },
    {
        "OMRID": 346352146,
        "name": "Henson Baird",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 118,
            "percentage": 98.5,
            "rank": 16
        },
        "chemistry": {
            "marks": 120,
            "percentage": 97,
            "rank": 21
        },
        "maths": {
            "marks": 107,
            "percentage": 98.68,
            "rank": 65
        },
        "total": {
            "marks": 309,
            "percentage": 99.51,
            "rank": 94
        }
    },
    {
        "OMRID": 426240147,
        "name": "Green Bird",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 115,
            "percentage": 95.87,
            "rank": 6
        },
        "chemistry": {
            "marks": 105,
            "percentage": 97.9,
            "rank": 35
        },
        "maths": {
            "marks": 115,
            "percentage": 95.47,
            "rank": 55
        },
        "total": {
            "marks": 340,
            "percentage": 97.92,
            "rank": 71
        }
    },
    {
        "OMRID": 711556148,
        "name": "Alta Carpenter",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 115,
            "percentage": 99.16,
            "rank": 30
        },
        "chemistry": {
            "marks": 103,
            "percentage": 95.53,
            "rank": 24
        },
        "maths": {
            "marks": 109,
            "percentage": 95.45,
            "rank": 52
        },
        "total": {
            "marks": 307,
            "percentage": 95.82,
            "rank": 41
        }
    },
    {
        "OMRID": 145083149,
        "name": "Hurley Noel",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 112,
            "percentage": 97.84,
            "rank": 13
        },
        "chemistry": {
            "marks": 111,
            "percentage": 96.86,
            "rank": 22
        },
        "maths": {
            "marks": 111,
            "percentage": 97.86,
            "rank": 11
        },
        "total": {
            "marks": 303,
            "percentage": 96.99,
            "rank": 19
        }
    },
    {
        "OMRID": 843257150,
        "name": "Spencer Ramirez",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 105,
            "percentage": 95.99,
            "rank": 93
        },
        "chemistry": {
            "marks": 119,
            "percentage": 98.85,
            "rank": 51
        },
        "maths": {
            "marks": 110,
            "percentage": 99.51,
            "rank": 13
        },
        "total": {
            "marks": 320,
            "percentage": 98.46,
            "rank": 11
        }
    },
    {
        "OMRID": 266347151,
        "name": "Beverly Doyle",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 109,
            "percentage": 95.61,
            "rank": 8
        },
        "chemistry": {
            "marks": 112,
            "percentage": 95.06,
            "rank": 26
        },
        "maths": {
            "marks": 114,
            "percentage": 99.06,
            "rank": 63
        },
        "total": {
            "marks": 306,
            "percentage": 96.08,
            "rank": 54
        }
    },
    {
        "OMRID": 494228152,
        "name": "Benton Dunn",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 120,
            "percentage": 97.86,
            "rank": 79
        },
        "chemistry": {
            "marks": 106,
            "percentage": 97.05,
            "rank": 6
        },
        "maths": {
            "marks": 110,
            "percentage": 95.24,
            "rank": 2
        },
        "total": {
            "marks": 356,
            "percentage": 95.22,
            "rank": 61
        }
    },
    {
        "OMRID": 761567153,
        "name": "Walters Gentry",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 100,
            "percentage": 96.25,
            "rank": 23
        },
        "chemistry": {
            "marks": 100,
            "percentage": 98.16,
            "rank": 41
        },
        "maths": {
            "marks": 113,
            "percentage": 97.78,
            "rank": 18
        },
        "total": {
            "marks": 323,
            "percentage": 95.05,
            "rank": 78
        }
    },
    {
        "OMRID": 219072154,
        "name": "Tyson Hudson",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 109,
            "percentage": 98.87,
            "rank": 99
        },
        "chemistry": {
            "marks": 113,
            "percentage": 95.51,
            "rank": 1
        },
        "maths": {
            "marks": 117,
            "percentage": 98.14,
            "rank": 59
        },
        "total": {
            "marks": 310,
            "percentage": 96.56,
            "rank": 11
        }
    },
    {
        "OMRID": 711373155,
        "name": "Melody Hoover",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 105,
            "percentage": 98.7,
            "rank": 60
        },
        "chemistry": {
            "marks": 105,
            "percentage": 97.93,
            "rank": 2
        },
        "maths": {
            "marks": 104,
            "percentage": 96.09,
            "rank": 30
        },
        "total": {
            "marks": 319,
            "percentage": 96.74,
            "rank": 39
        }
    },
    {
        "OMRID": 687836156,
        "name": "Deanne Avila",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 108,
            "percentage": 95.81,
            "rank": 98
        },
        "chemistry": {
            "marks": 105,
            "percentage": 96.11,
            "rank": 36
        },
        "maths": {
            "marks": 102,
            "percentage": 97.02,
            "rank": 43
        },
        "total": {
            "marks": 316,
            "percentage": 99.64,
            "rank": 86
        }
    },
    {
        "OMRID": 239922157,
        "name": "Francis Mcdaniel",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 118,
            "percentage": 97.09,
            "rank": 6
        },
        "chemistry": {
            "marks": 118,
            "percentage": 99.71,
            "rank": 36
        },
        "maths": {
            "marks": 118,
            "percentage": 95.75,
            "rank": 69
        },
        "total": {
            "marks": 331,
            "percentage": 98.38,
            "rank": 95
        }
    },
    {
        "OMRID": 222663158,
        "name": "Bernard Parks",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 115,
            "percentage": 97.49,
            "rank": 100
        },
        "chemistry": {
            "marks": 104,
            "percentage": 98.31,
            "rank": 3
        },
        "maths": {
            "marks": 102,
            "percentage": 98.72,
            "rank": 23
        },
        "total": {
            "marks": 346,
            "percentage": 98.66,
            "rank": 47
        }
    },
    {
        "OMRID": 323956159,
        "name": "Sallie Williamson",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 117,
            "percentage": 98.39,
            "rank": 56
        },
        "chemistry": {
            "marks": 113,
            "percentage": 99.31,
            "rank": 87
        },
        "maths": {
            "marks": 113,
            "percentage": 97.23,
            "rank": 90
        },
        "total": {
            "marks": 316,
            "percentage": 95.09,
            "rank": 53
        }
    },
    {
        "OMRID": 930731160,
        "name": "Lynnette Armstrong",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 114,
            "percentage": 99.36,
            "rank": 96
        },
        "chemistry": {
            "marks": 102,
            "percentage": 99.06,
            "rank": 72
        },
        "maths": {
            "marks": 104,
            "percentage": 97.19,
            "rank": 99
        },
        "total": {
            "marks": 328,
            "percentage": 96.44,
            "rank": 65
        }
    },
    {
        "OMRID": 796075161,
        "name": "Martina Gill",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 107,
            "percentage": 97.58,
            "rank": 86
        },
        "chemistry": {
            "marks": 114,
            "percentage": 99.86,
            "rank": 43
        },
        "maths": {
            "marks": 114,
            "percentage": 99.55,
            "rank": 11
        },
        "total": {
            "marks": 320,
            "percentage": 99.2,
            "rank": 100
        }
    },
    {
        "OMRID": 117462162,
        "name": "Ashlee Neal",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 114,
            "percentage": 95.61,
            "rank": 73
        },
        "chemistry": {
            "marks": 109,
            "percentage": 96.5,
            "rank": 65
        },
        "maths": {
            "marks": 118,
            "percentage": 95.08,
            "rank": 96
        },
        "total": {
            "marks": 306,
            "percentage": 96.35,
            "rank": 69
        }
    },
    {
        "OMRID": 392349163,
        "name": "Dollie Cabrera",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 109,
            "percentage": 98.71,
            "rank": 25
        },
        "chemistry": {
            "marks": 106,
            "percentage": 97.03,
            "rank": 37
        },
        "maths": {
            "marks": 118,
            "percentage": 95.12,
            "rank": 95
        },
        "total": {
            "marks": 326,
            "percentage": 98.74,
            "rank": 99
        }
    },
    {
        "OMRID": 403970164,
        "name": "Gibson Cox",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 114,
            "percentage": 96.89,
            "rank": 9
        },
        "chemistry": {
            "marks": 117,
            "percentage": 96.46,
            "rank": 76
        },
        "maths": {
            "marks": 111,
            "percentage": 95.21,
            "rank": 19
        },
        "total": {
            "marks": 328,
            "percentage": 95.48,
            "rank": 14
        }
    },
    {
        "OMRID": 736218165,
        "name": "Catalina Barnes",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 101,
            "percentage": 96.58,
            "rank": 39
        },
        "chemistry": {
            "marks": 108,
            "percentage": 99.89,
            "rank": 99
        },
        "maths": {
            "marks": 106,
            "percentage": 98.23,
            "rank": 58
        },
        "total": {
            "marks": 326,
            "percentage": 99.13,
            "rank": 96
        }
    },
    {
        "OMRID": 684628166,
        "name": "Traci Barker",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 117,
            "percentage": 99.63,
            "rank": 60
        },
        "chemistry": {
            "marks": 111,
            "percentage": 96.89,
            "rank": 93
        },
        "maths": {
            "marks": 101,
            "percentage": 97.27,
            "rank": 42
        },
        "total": {
            "marks": 317,
            "percentage": 98.43,
            "rank": 71
        }
    },
    {
        "OMRID": 492983167,
        "name": "Brandy Jones",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 113,
            "percentage": 95.99,
            "rank": 37
        },
        "chemistry": {
            "marks": 116,
            "percentage": 99.63,
            "rank": 23
        },
        "maths": {
            "marks": 106,
            "percentage": 97.16,
            "rank": 67
        },
        "total": {
            "marks": 308,
            "percentage": 98.86,
            "rank": 93
        }
    },
    {
        "OMRID": 310518168,
        "name": "Ida Hardy",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 106,
            "percentage": 95.3,
            "rank": 46
        },
        "chemistry": {
            "marks": 103,
            "percentage": 96.27,
            "rank": 26
        },
        "maths": {
            "marks": 115,
            "percentage": 98.58,
            "rank": 81
        },
        "total": {
            "marks": 327,
            "percentage": 99.4,
            "rank": 83
        }
    },
    {
        "OMRID": 465551169,
        "name": "Montoya Morton",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 110,
            "percentage": 99.5,
            "rank": 3
        },
        "chemistry": {
            "marks": 111,
            "percentage": 95.04,
            "rank": 43
        },
        "maths": {
            "marks": 115,
            "percentage": 95.04,
            "rank": 15
        },
        "total": {
            "marks": 304,
            "percentage": 98.23,
            "rank": 15
        }
    },
    {
        "OMRID": 784236170,
        "name": "Audrey Casey",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 100,
            "percentage": 97.31,
            "rank": 76
        },
        "chemistry": {
            "marks": 100,
            "percentage": 97.29,
            "rank": 83
        },
        "maths": {
            "marks": 101,
            "percentage": 98.65,
            "rank": 4
        },
        "total": {
            "marks": 338,
            "percentage": 97.45,
            "rank": 1
        }
    },
    {
        "OMRID": 362215171,
        "name": "Blanca Herman",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 119,
            "percentage": 97.65,
            "rank": 76
        },
        "chemistry": {
            "marks": 104,
            "percentage": 96.53,
            "rank": 43
        },
        "maths": {
            "marks": 107,
            "percentage": 97.16,
            "rank": 57
        },
        "total": {
            "marks": 304,
            "percentage": 98.78,
            "rank": 80
        }
    },
    {
        "OMRID": 740366172,
        "name": "Mccall Bullock",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 111,
            "percentage": 99.27,
            "rank": 93
        },
        "chemistry": {
            "marks": 103,
            "percentage": 95.3,
            "rank": 46
        },
        "maths": {
            "marks": 103,
            "percentage": 95.93,
            "rank": 1
        },
        "total": {
            "marks": 358,
            "percentage": 95.15,
            "rank": 79
        }
    },
    {
        "OMRID": 905986173,
        "name": "Mara Bowers",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 119,
            "percentage": 97.06,
            "rank": 2
        },
        "chemistry": {
            "marks": 112,
            "percentage": 95.43,
            "rank": 63
        },
        "maths": {
            "marks": 110,
            "percentage": 99.27,
            "rank": 98
        },
        "total": {
            "marks": 323,
            "percentage": 98.37,
            "rank": 63
        }
    },
    {
        "OMRID": 176560174,
        "name": "Joann York",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 113,
            "percentage": 99.16,
            "rank": 50
        },
        "chemistry": {
            "marks": 117,
            "percentage": 98.8,
            "rank": 38
        },
        "maths": {
            "marks": 111,
            "percentage": 99.13,
            "rank": 41
        },
        "total": {
            "marks": 340,
            "percentage": 98.37,
            "rank": 95
        }
    },
    {
        "OMRID": 551112175,
        "name": "Deleon Gutierrez",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 117,
            "percentage": 99.02,
            "rank": 98
        },
        "chemistry": {
            "marks": 101,
            "percentage": 95.4,
            "rank": 27
        },
        "maths": {
            "marks": 101,
            "percentage": 98.7,
            "rank": 17
        },
        "total": {
            "marks": 340,
            "percentage": 98.08,
            "rank": 66
        }
    },
    {
        "OMRID": 751894176,
        "name": "Walls Mccarthy",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 113,
            "percentage": 98.83,
            "rank": 82
        },
        "chemistry": {
            "marks": 120,
            "percentage": 96.92,
            "rank": 88
        },
        "maths": {
            "marks": 120,
            "percentage": 96.23,
            "rank": 51
        },
        "total": {
            "marks": 310,
            "percentage": 95.35,
            "rank": 11
        }
    },
    {
        "OMRID": 770611177,
        "name": "Maggie Wade",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 104,
            "percentage": 95.45,
            "rank": 70
        },
        "chemistry": {
            "marks": 112,
            "percentage": 99,
            "rank": 8
        },
        "maths": {
            "marks": 115,
            "percentage": 99.56,
            "rank": 18
        },
        "total": {
            "marks": 336,
            "percentage": 96.9,
            "rank": 70
        }
    },
    {
        "OMRID": 284331178,
        "name": "Tonya Mendez",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 116,
            "percentage": 99.23,
            "rank": 70
        },
        "chemistry": {
            "marks": 100,
            "percentage": 97.45,
            "rank": 40
        },
        "maths": {
            "marks": 112,
            "percentage": 99.92,
            "rank": 34
        },
        "total": {
            "marks": 343,
            "percentage": 98.91,
            "rank": 24
        }
    },
    {
        "OMRID": 412789179,
        "name": "Cohen Sharp",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 105,
            "percentage": 98.15,
            "rank": 45
        },
        "chemistry": {
            "marks": 109,
            "percentage": 98.44,
            "rank": 12
        },
        "maths": {
            "marks": 120,
            "percentage": 96.19,
            "rank": 7
        },
        "total": {
            "marks": 306,
            "percentage": 98.66,
            "rank": 20
        }
    },
    {
        "OMRID": 676952180,
        "name": "Nicole Brady",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 104,
            "percentage": 95.59,
            "rank": 26
        },
        "chemistry": {
            "marks": 117,
            "percentage": 99.12,
            "rank": 89
        },
        "maths": {
            "marks": 112,
            "percentage": 99.34,
            "rank": 42
        },
        "total": {
            "marks": 328,
            "percentage": 97.42,
            "rank": 45
        }
    },
    {
        "OMRID": 816988181,
        "name": "Hopper Bates",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 111,
            "percentage": 96.51,
            "rank": 87
        },
        "chemistry": {
            "marks": 118,
            "percentage": 96.42,
            "rank": 28
        },
        "maths": {
            "marks": 113,
            "percentage": 95.57,
            "rank": 49
        },
        "total": {
            "marks": 359,
            "percentage": 99.81,
            "rank": 96
        }
    },
    {
        "OMRID": 307589182,
        "name": "Clare Rowe",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 112,
            "percentage": 97.51,
            "rank": 43
        },
        "chemistry": {
            "marks": 120,
            "percentage": 95.95,
            "rank": 12
        },
        "maths": {
            "marks": 110,
            "percentage": 97.06,
            "rank": 83
        },
        "total": {
            "marks": 322,
            "percentage": 99.95,
            "rank": 33
        }
    },
    {
        "OMRID": 755685183,
        "name": "Levy Reid",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 111,
            "percentage": 95.16,
            "rank": 72
        },
        "chemistry": {
            "marks": 118,
            "percentage": 95.72,
            "rank": 4
        },
        "maths": {
            "marks": 110,
            "percentage": 97.91,
            "rank": 75
        },
        "total": {
            "marks": 356,
            "percentage": 95.99,
            "rank": 86
        }
    },
    {
        "OMRID": 974097184,
        "name": "Odessa Farmer",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 115,
            "percentage": 97.8,
            "rank": 36
        },
        "chemistry": {
            "marks": 116,
            "percentage": 97.35,
            "rank": 57
        },
        "maths": {
            "marks": 102,
            "percentage": 97.6,
            "rank": 90
        },
        "total": {
            "marks": 323,
            "percentage": 95.19,
            "rank": 95
        }
    },
    {
        "OMRID": 522744185,
        "name": "Sampson Hinton",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 111,
            "percentage": 99.32,
            "rank": 63
        },
        "chemistry": {
            "marks": 110,
            "percentage": 99.93,
            "rank": 74
        },
        "maths": {
            "marks": 106,
            "percentage": 97.9,
            "rank": 98
        },
        "total": {
            "marks": 322,
            "percentage": 96.61,
            "rank": 20
        }
    },
    {
        "OMRID": 442937186,
        "name": "Deanna Prince",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 100,
            "percentage": 99.57,
            "rank": 34
        },
        "chemistry": {
            "marks": 100,
            "percentage": 97.09,
            "rank": 53
        },
        "maths": {
            "marks": 116,
            "percentage": 99.01,
            "rank": 36
        },
        "total": {
            "marks": 324,
            "percentage": 98.55,
            "rank": 16
        }
    },
    {
        "OMRID": 644309187,
        "name": "English Ferguson",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 100,
            "percentage": 97.8,
            "rank": 16
        },
        "chemistry": {
            "marks": 113,
            "percentage": 99.77,
            "rank": 88
        },
        "maths": {
            "marks": 120,
            "percentage": 95.42,
            "rank": 94
        },
        "total": {
            "marks": 308,
            "percentage": 97.19,
            "rank": 38
        }
    },
    {
        "OMRID": 373638188,
        "name": "Candy Frederick",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 116,
            "percentage": 98.92,
            "rank": 16
        },
        "chemistry": {
            "marks": 113,
            "percentage": 99.13,
            "rank": 14
        },
        "maths": {
            "marks": 108,
            "percentage": 96.83,
            "rank": 79
        },
        "total": {
            "marks": 304,
            "percentage": 96.65,
            "rank": 93
        }
    },
    {
        "OMRID": 774571189,
        "name": "Madge Wilkins",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 100,
            "percentage": 95.66,
            "rank": 6
        },
        "chemistry": {
            "marks": 113,
            "percentage": 98.36,
            "rank": 35
        },
        "maths": {
            "marks": 106,
            "percentage": 99.99,
            "rank": 97
        },
        "total": {
            "marks": 312,
            "percentage": 97.59,
            "rank": 35
        }
    },
    {
        "OMRID": 600218190,
        "name": "Foster Puckett",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 108,
            "percentage": 96.77,
            "rank": 57
        },
        "chemistry": {
            "marks": 110,
            "percentage": 95.1,
            "rank": 30
        },
        "maths": {
            "marks": 112,
            "percentage": 97.87,
            "rank": 68
        },
        "total": {
            "marks": 344,
            "percentage": 98.32,
            "rank": 27
        }
    },
    {
        "OMRID": 398725191,
        "name": "Arlene Harrington",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 103,
            "percentage": 98.75,
            "rank": 65
        },
        "chemistry": {
            "marks": 105,
            "percentage": 96.49,
            "rank": 84
        },
        "maths": {
            "marks": 105,
            "percentage": 97.89,
            "rank": 95
        },
        "total": {
            "marks": 306,
            "percentage": 96.18,
            "rank": 43
        }
    },
    {
        "OMRID": 526700192,
        "name": "Rhonda Lewis",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 107,
            "percentage": 98.22,
            "rank": 45
        },
        "chemistry": {
            "marks": 105,
            "percentage": 97.65,
            "rank": 83
        },
        "maths": {
            "marks": 104,
            "percentage": 96.84,
            "rank": 61
        },
        "total": {
            "marks": 332,
            "percentage": 98.94,
            "rank": 6
        }
    },
    {
        "OMRID": 723209193,
        "name": "Erin Atkins",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 106,
            "percentage": 95.24,
            "rank": 77
        },
        "chemistry": {
            "marks": 116,
            "percentage": 95.22,
            "rank": 95
        },
        "maths": {
            "marks": 108,
            "percentage": 99.55,
            "rank": 51
        },
        "total": {
            "marks": 307,
            "percentage": 98.66,
            "rank": 22
        }
    },
    {
        "OMRID": 978440194,
        "name": "Pamela Downs",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 107,
            "percentage": 99.84,
            "rank": 32
        },
        "chemistry": {
            "marks": 108,
            "percentage": 97.51,
            "rank": 11
        },
        "maths": {
            "marks": 118,
            "percentage": 95.42,
            "rank": 66
        },
        "total": {
            "marks": 341,
            "percentage": 99.52,
            "rank": 66
        }
    },
    {
        "OMRID": 878444195,
        "name": "Jessie Powell",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 114,
            "percentage": 97.45,
            "rank": 40
        },
        "chemistry": {
            "marks": 107,
            "percentage": 96.34,
            "rank": 34
        },
        "maths": {
            "marks": 113,
            "percentage": 95.78,
            "rank": 39
        },
        "total": {
            "marks": 352,
            "percentage": 95.76,
            "rank": 8
        }
    },
    {
        "OMRID": 958078196,
        "name": "Dale Avery",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 116,
            "percentage": 99.3,
            "rank": 59
        },
        "chemistry": {
            "marks": 113,
            "percentage": 98.35,
            "rank": 89
        },
        "maths": {
            "marks": 113,
            "percentage": 98.15,
            "rank": 18
        },
        "total": {
            "marks": 335,
            "percentage": 95.85,
            "rank": 3
        }
    },
    {
        "OMRID": 366736197,
        "name": "Theresa Gonzales",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 108,
            "percentage": 97.9,
            "rank": 100
        },
        "chemistry": {
            "marks": 103,
            "percentage": 98.86,
            "rank": 40
        },
        "maths": {
            "marks": 113,
            "percentage": 97.93,
            "rank": 87
        },
        "total": {
            "marks": 331,
            "percentage": 97.51,
            "rank": 27
        }
    },
    {
        "OMRID": 851880198,
        "name": "Maryann Kelly",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 115,
            "percentage": 97.73,
            "rank": 31
        },
        "chemistry": {
            "marks": 108,
            "percentage": 96.05,
            "rank": 18
        },
        "maths": {
            "marks": 109,
            "percentage": 96.35,
            "rank": 25
        },
        "total": {
            "marks": 323,
            "percentage": 96.27,
            "rank": 77
        }
    },
    {
        "OMRID": 330164199,
        "name": "Suzanne Hogan",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 106,
            "percentage": 99.42,
            "rank": 21
        },
        "chemistry": {
            "marks": 101,
            "percentage": 99.53,
            "rank": 71
        },
        "maths": {
            "marks": 113,
            "percentage": 95.1,
            "rank": 14
        },
        "total": {
            "marks": 324,
            "percentage": 97.91,
            "rank": 33
        }
    },
    {
        "OMRID": 109014200,
        "name": "Marcie Burris",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 120,
            "percentage": 99.84,
            "rank": 35
        },
        "chemistry": {
            "marks": 100,
            "percentage": 95.17,
            "rank": 79
        },
        "maths": {
            "marks": 111,
            "percentage": 97.72,
            "rank": 39
        },
        "total": {
            "marks": 337,
            "percentage": 98.17,
            "rank": 38
        }
    },
    {
        "OMRID": 961902201,
        "name": "Mcknight Freeman",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 113,
            "percentage": 97.87,
            "rank": 77
        },
        "chemistry": {
            "marks": 106,
            "percentage": 97.45,
            "rank": 55
        },
        "maths": {
            "marks": 106,
            "percentage": 97.85,
            "rank": 2
        },
        "total": {
            "marks": 341,
            "percentage": 99.58,
            "rank": 43
        }
    },
    {
        "OMRID": 170470202,
        "name": "Bartlett Mcfadden",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 118,
            "percentage": 95.91,
            "rank": 24
        },
        "chemistry": {
            "marks": 104,
            "percentage": 95.95,
            "rank": 89
        },
        "maths": {
            "marks": 103,
            "percentage": 95.26,
            "rank": 6
        },
        "total": {
            "marks": 348,
            "percentage": 95.33,
            "rank": 55
        }
    },
    {
        "OMRID": 175024203,
        "name": "Horne Guerra",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 119,
            "percentage": 96.37,
            "rank": 82
        },
        "chemistry": {
            "marks": 108,
            "percentage": 97.75,
            "rank": 35
        },
        "maths": {
            "marks": 100,
            "percentage": 98.59,
            "rank": 89
        },
        "total": {
            "marks": 355,
            "percentage": 96.76,
            "rank": 70
        }
    },
    {
        "OMRID": 303750204,
        "name": "Ebony Barber",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 116,
            "percentage": 95.2,
            "rank": 96
        },
        "chemistry": {
            "marks": 119,
            "percentage": 99.38,
            "rank": 34
        },
        "maths": {
            "marks": 102,
            "percentage": 95.15,
            "rank": 23
        },
        "total": {
            "marks": 330,
            "percentage": 95.31,
            "rank": 49
        }
    },
    {
        "OMRID": 103884205,
        "name": "Carr Bentley",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 108,
            "percentage": 98.29,
            "rank": 99
        },
        "chemistry": {
            "marks": 106,
            "percentage": 96.38,
            "rank": 81
        },
        "maths": {
            "marks": 110,
            "percentage": 95.01,
            "rank": 79
        },
        "total": {
            "marks": 345,
            "percentage": 96.22,
            "rank": 91
        }
    },
    {
        "OMRID": 994352206,
        "name": "Ingram Peters",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 112,
            "percentage": 95.27,
            "rank": 96
        },
        "chemistry": {
            "marks": 119,
            "percentage": 95.73,
            "rank": 100
        },
        "maths": {
            "marks": 108,
            "percentage": 95.26,
            "rank": 79
        },
        "total": {
            "marks": 306,
            "percentage": 98.27,
            "rank": 36
        }
    },
    {
        "OMRID": 462470207,
        "name": "Barnes Terrell",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 113,
            "percentage": 96.69,
            "rank": 72
        },
        "chemistry": {
            "marks": 103,
            "percentage": 98.7,
            "rank": 21
        },
        "maths": {
            "marks": 104,
            "percentage": 99.36,
            "rank": 93
        },
        "total": {
            "marks": 314,
            "percentage": 96.14,
            "rank": 8
        }
    },
    {
        "OMRID": 178469208,
        "name": "Holloway Randolph",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 100,
            "percentage": 99.42,
            "rank": 10
        },
        "chemistry": {
            "marks": 117,
            "percentage": 95.43,
            "rank": 54
        },
        "maths": {
            "marks": 112,
            "percentage": 98.23,
            "rank": 73
        },
        "total": {
            "marks": 311,
            "percentage": 98.45,
            "rank": 64
        }
    },
    {
        "OMRID": 624183209,
        "name": "Ollie Bean",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 106,
            "percentage": 97.85,
            "rank": 58
        },
        "chemistry": {
            "marks": 109,
            "percentage": 96.07,
            "rank": 83
        },
        "maths": {
            "marks": 120,
            "percentage": 99.54,
            "rank": 94
        },
        "total": {
            "marks": 308,
            "percentage": 97.37,
            "rank": 41
        }
    },
    {
        "OMRID": 606511210,
        "name": "Fern Calhoun",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 109,
            "percentage": 99.07,
            "rank": 81
        },
        "chemistry": {
            "marks": 115,
            "percentage": 96.93,
            "rank": 29
        },
        "maths": {
            "marks": 107,
            "percentage": 98.99,
            "rank": 11
        },
        "total": {
            "marks": 341,
            "percentage": 98.83,
            "rank": 81
        }
    },
    {
        "OMRID": 443440211,
        "name": "Flora Holloway",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 107,
            "percentage": 95.48,
            "rank": 8
        },
        "chemistry": {
            "marks": 113,
            "percentage": 98.02,
            "rank": 89
        },
        "maths": {
            "marks": 119,
            "percentage": 95.65,
            "rank": 74
        },
        "total": {
            "marks": 340,
            "percentage": 96.36,
            "rank": 21
        }
    },
    {
        "OMRID": 511216212,
        "name": "Evangelina Taylor",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 101,
            "percentage": 96.33,
            "rank": 21
        },
        "chemistry": {
            "marks": 120,
            "percentage": 98.35,
            "rank": 43
        },
        "maths": {
            "marks": 104,
            "percentage": 98.43,
            "rank": 27
        },
        "total": {
            "marks": 354,
            "percentage": 96.68,
            "rank": 46
        }
    },
    {
        "OMRID": 358131213,
        "name": "Freeman Rodriquez",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 100,
            "percentage": 99.87,
            "rank": 24
        },
        "chemistry": {
            "marks": 117,
            "percentage": 96.32,
            "rank": 90
        },
        "maths": {
            "marks": 114,
            "percentage": 96.8,
            "rank": 96
        },
        "total": {
            "marks": 308,
            "percentage": 97.76,
            "rank": 38
        }
    },
    {
        "OMRID": 671558214,
        "name": "Mccullough Parrish",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 117,
            "percentage": 98.09,
            "rank": 91
        },
        "chemistry": {
            "marks": 119,
            "percentage": 98.79,
            "rank": 76
        },
        "maths": {
            "marks": 117,
            "percentage": 97.27,
            "rank": 59
        },
        "total": {
            "marks": 329,
            "percentage": 97.09,
            "rank": 52
        }
    },
    {
        "OMRID": 965886215,
        "name": "Vang White",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 101,
            "percentage": 96.13,
            "rank": 94
        },
        "chemistry": {
            "marks": 119,
            "percentage": 96.4,
            "rank": 62
        },
        "maths": {
            "marks": 106,
            "percentage": 97.23,
            "rank": 75
        },
        "total": {
            "marks": 322,
            "percentage": 98.07,
            "rank": 32
        }
    },
    {
        "OMRID": 147093216,
        "name": "Whitfield Cotton",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 106,
            "percentage": 98.4,
            "rank": 91
        },
        "chemistry": {
            "marks": 110,
            "percentage": 95.24,
            "rank": 40
        },
        "maths": {
            "marks": 120,
            "percentage": 97.48,
            "rank": 27
        },
        "total": {
            "marks": 309,
            "percentage": 98.56,
            "rank": 29
        }
    },
    {
        "OMRID": 287669217,
        "name": "Leticia Fleming",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 112,
            "percentage": 95.09,
            "rank": 20
        },
        "chemistry": {
            "marks": 105,
            "percentage": 99.05,
            "rank": 1
        },
        "maths": {
            "marks": 109,
            "percentage": 96.26,
            "rank": 32
        },
        "total": {
            "marks": 329,
            "percentage": 99.29,
            "rank": 37
        }
    },
    {
        "OMRID": 105238218,
        "name": "Ruiz Fitzgerald",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 120,
            "percentage": 95.15,
            "rank": 96
        },
        "chemistry": {
            "marks": 115,
            "percentage": 98.13,
            "rank": 58
        },
        "maths": {
            "marks": 104,
            "percentage": 97.56,
            "rank": 66
        },
        "total": {
            "marks": 304,
            "percentage": 99.23,
            "rank": 74
        }
    },
    {
        "OMRID": 294765219,
        "name": "Alyson Briggs",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 109,
            "percentage": 98.41,
            "rank": 28
        },
        "chemistry": {
            "marks": 109,
            "percentage": 98.27,
            "rank": 77
        },
        "maths": {
            "marks": 111,
            "percentage": 97.81,
            "rank": 39
        },
        "total": {
            "marks": 347,
            "percentage": 97.05,
            "rank": 28
        }
    },
    {
        "OMRID": 836010220,
        "name": "James Deleon",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 105,
            "percentage": 99.12,
            "rank": 36
        },
        "chemistry": {
            "marks": 100,
            "percentage": 95.37,
            "rank": 74
        },
        "maths": {
            "marks": 100,
            "percentage": 99.73,
            "rank": 59
        },
        "total": {
            "marks": 347,
            "percentage": 95.96,
            "rank": 40
        }
    },
    {
        "OMRID": 284446221,
        "name": "Shelia Haney",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 109,
            "percentage": 98.39,
            "rank": 41
        },
        "chemistry": {
            "marks": 110,
            "percentage": 99.28,
            "rank": 90
        },
        "maths": {
            "marks": 101,
            "percentage": 95.81,
            "rank": 93
        },
        "total": {
            "marks": 327,
            "percentage": 97.61,
            "rank": 31
        }
    },
    {
        "OMRID": 416136222,
        "name": "Tucker Campos",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 116,
            "percentage": 96.23,
            "rank": 16
        },
        "chemistry": {
            "marks": 112,
            "percentage": 95.12,
            "rank": 14
        },
        "maths": {
            "marks": 116,
            "percentage": 98.64,
            "rank": 46
        },
        "total": {
            "marks": 310,
            "percentage": 95.18,
            "rank": 25
        }
    },
    {
        "OMRID": 445692223,
        "name": "Whitney Mathis",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 106,
            "percentage": 95.79,
            "rank": 73
        },
        "chemistry": {
            "marks": 108,
            "percentage": 98.71,
            "rank": 37
        },
        "maths": {
            "marks": 116,
            "percentage": 99.41,
            "rank": 2
        },
        "total": {
            "marks": 353,
            "percentage": 97.22,
            "rank": 54
        }
    },
    {
        "OMRID": 245718224,
        "name": "Knowles Joyce",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 115,
            "percentage": 97.21,
            "rank": 26
        },
        "chemistry": {
            "marks": 119,
            "percentage": 96.61,
            "rank": 64
        },
        "maths": {
            "marks": 103,
            "percentage": 96.33,
            "rank": 92
        },
        "total": {
            "marks": 344,
            "percentage": 99.15,
            "rank": 80
        }
    },
    {
        "OMRID": 431952225,
        "name": "Sullivan Silva",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 104,
            "percentage": 97.74,
            "rank": 98
        },
        "chemistry": {
            "marks": 109,
            "percentage": 98.95,
            "rank": 27
        },
        "maths": {
            "marks": 113,
            "percentage": 95.63,
            "rank": 38
        },
        "total": {
            "marks": 322,
            "percentage": 99.27,
            "rank": 17
        }
    },
    {
        "OMRID": 683634226,
        "name": "Connie Mann",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 107,
            "percentage": 96.96,
            "rank": 99
        },
        "chemistry": {
            "marks": 118,
            "percentage": 96.07,
            "rank": 61
        },
        "maths": {
            "marks": 116,
            "percentage": 98.33,
            "rank": 19
        },
        "total": {
            "marks": 323,
            "percentage": 97.97,
            "rank": 62
        }
    },
    {
        "OMRID": 650762227,
        "name": "Lorie Ford",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 117,
            "percentage": 99.25,
            "rank": 47
        },
        "chemistry": {
            "marks": 117,
            "percentage": 96.29,
            "rank": 32
        },
        "maths": {
            "marks": 117,
            "percentage": 96.98,
            "rank": 41
        },
        "total": {
            "marks": 334,
            "percentage": 98.68,
            "rank": 82
        }
    },
    {
        "OMRID": 194142228,
        "name": "Pratt Pratt",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 117,
            "percentage": 97.7,
            "rank": 38
        },
        "chemistry": {
            "marks": 103,
            "percentage": 97.71,
            "rank": 6
        },
        "maths": {
            "marks": 119,
            "percentage": 97.45,
            "rank": 28
        },
        "total": {
            "marks": 313,
            "percentage": 97.13,
            "rank": 66
        }
    },
    {
        "OMRID": 154191229,
        "name": "Ray Wells",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 114,
            "percentage": 98.26,
            "rank": 93
        },
        "chemistry": {
            "marks": 113,
            "percentage": 96.36,
            "rank": 65
        },
        "maths": {
            "marks": 107,
            "percentage": 96.92,
            "rank": 76
        },
        "total": {
            "marks": 327,
            "percentage": 95.06,
            "rank": 77
        }
    },
    {
        "OMRID": 302100230,
        "name": "Gina Mccullough",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 112,
            "percentage": 95.59,
            "rank": 100
        },
        "chemistry": {
            "marks": 106,
            "percentage": 97.95,
            "rank": 98
        },
        "maths": {
            "marks": 102,
            "percentage": 95.88,
            "rank": 15
        },
        "total": {
            "marks": 306,
            "percentage": 96.41,
            "rank": 27
        }
    },
    {
        "OMRID": 773753231,
        "name": "Rosario Chapman",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 117,
            "percentage": 99.98,
            "rank": 77
        },
        "chemistry": {
            "marks": 108,
            "percentage": 97.39,
            "rank": 7
        },
        "maths": {
            "marks": 106,
            "percentage": 95.94,
            "rank": 52
        },
        "total": {
            "marks": 337,
            "percentage": 97.58,
            "rank": 26
        }
    },
    {
        "OMRID": 752805232,
        "name": "Patton Christian",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 110,
            "percentage": 95,
            "rank": 72
        },
        "chemistry": {
            "marks": 116,
            "percentage": 97.36,
            "rank": 79
        },
        "maths": {
            "marks": 118,
            "percentage": 96.27,
            "rank": 52
        },
        "total": {
            "marks": 339,
            "percentage": 97.75,
            "rank": 98
        }
    },
    {
        "OMRID": 752411233,
        "name": "Cora Baxter",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 102,
            "percentage": 99.12,
            "rank": 47
        },
        "chemistry": {
            "marks": 104,
            "percentage": 95.93,
            "rank": 91
        },
        "maths": {
            "marks": 110,
            "percentage": 99.08,
            "rank": 71
        },
        "total": {
            "marks": 317,
            "percentage": 95.71,
            "rank": 15
        }
    },
    {
        "OMRID": 170606234,
        "name": "Lillian Monroe",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 115,
            "percentage": 99.73,
            "rank": 3
        },
        "chemistry": {
            "marks": 102,
            "percentage": 99.45,
            "rank": 68
        },
        "maths": {
            "marks": 117,
            "percentage": 97.78,
            "rank": 1
        },
        "total": {
            "marks": 345,
            "percentage": 99.29,
            "rank": 64
        }
    },
    {
        "OMRID": 485415235,
        "name": "Shawna Benjamin",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 107,
            "percentage": 99.18,
            "rank": 75
        },
        "chemistry": {
            "marks": 109,
            "percentage": 97.19,
            "rank": 14
        },
        "maths": {
            "marks": 120,
            "percentage": 97.46,
            "rank": 81
        },
        "total": {
            "marks": 339,
            "percentage": 95.81,
            "rank": 5
        }
    },
    {
        "OMRID": 732957236,
        "name": "Curry Ellison",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 115,
            "percentage": 95.03,
            "rank": 48
        },
        "chemistry": {
            "marks": 116,
            "percentage": 97.5,
            "rank": 94
        },
        "maths": {
            "marks": 108,
            "percentage": 99.87,
            "rank": 59
        },
        "total": {
            "marks": 331,
            "percentage": 99.78,
            "rank": 90
        }
    },
    {
        "OMRID": 369738237,
        "name": "Peck Little",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 120,
            "percentage": 95.71,
            "rank": 59
        },
        "chemistry": {
            "marks": 110,
            "percentage": 98.86,
            "rank": 95
        },
        "maths": {
            "marks": 110,
            "percentage": 97.65,
            "rank": 88
        },
        "total": {
            "marks": 352,
            "percentage": 95.78,
            "rank": 97
        }
    },
    {
        "OMRID": 619550238,
        "name": "Benita Barry",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 100,
            "percentage": 97.71,
            "rank": 57
        },
        "chemistry": {
            "marks": 103,
            "percentage": 99.46,
            "rank": 67
        },
        "maths": {
            "marks": 110,
            "percentage": 96.61,
            "rank": 67
        },
        "total": {
            "marks": 335,
            "percentage": 96.05,
            "rank": 7
        }
    },
    {
        "OMRID": 720447239,
        "name": "Campbell Garcia",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 116,
            "percentage": 99.72,
            "rank": 44
        },
        "chemistry": {
            "marks": 104,
            "percentage": 98.37,
            "rank": 14
        },
        "maths": {
            "marks": 104,
            "percentage": 96.52,
            "rank": 35
        },
        "total": {
            "marks": 321,
            "percentage": 98.09,
            "rank": 33
        }
    },
    {
        "OMRID": 932653240,
        "name": "Wynn Copeland",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 117,
            "percentage": 99.63,
            "rank": 84
        },
        "chemistry": {
            "marks": 110,
            "percentage": 95.4,
            "rank": 66
        },
        "maths": {
            "marks": 107,
            "percentage": 97.74,
            "rank": 1
        },
        "total": {
            "marks": 319,
            "percentage": 99.14,
            "rank": 98
        }
    },
    {
        "OMRID": 131429241,
        "name": "Beck Ortega",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 112,
            "percentage": 99.4,
            "rank": 54
        },
        "chemistry": {
            "marks": 109,
            "percentage": 95.18,
            "rank": 27
        },
        "maths": {
            "marks": 109,
            "percentage": 98.41,
            "rank": 87
        },
        "total": {
            "marks": 313,
            "percentage": 95.78,
            "rank": 91
        }
    },
    {
        "OMRID": 324124242,
        "name": "Roseann Salinas",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 102,
            "percentage": 97.64,
            "rank": 94
        },
        "chemistry": {
            "marks": 114,
            "percentage": 96.22,
            "rank": 26
        },
        "maths": {
            "marks": 116,
            "percentage": 95.17,
            "rank": 47
        },
        "total": {
            "marks": 358,
            "percentage": 98.82,
            "rank": 41
        }
    },
    {
        "OMRID": 435569243,
        "name": "Gonzalez Knowles",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 118,
            "percentage": 98.37,
            "rank": 59
        },
        "chemistry": {
            "marks": 113,
            "percentage": 97.15,
            "rank": 62
        },
        "maths": {
            "marks": 120,
            "percentage": 99.79,
            "rank": 43
        },
        "total": {
            "marks": 302,
            "percentage": 96.36,
            "rank": 39
        }
    },
    {
        "OMRID": 296292244,
        "name": "Bernadine Powers",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 116,
            "percentage": 96.81,
            "rank": 57
        },
        "chemistry": {
            "marks": 102,
            "percentage": 95.36,
            "rank": 46
        },
        "maths": {
            "marks": 108,
            "percentage": 99.79,
            "rank": 21
        },
        "total": {
            "marks": 340,
            "percentage": 97.69,
            "rank": 77
        }
    },
    {
        "OMRID": 811941245,
        "name": "Berta Sargent",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 114,
            "percentage": 96.14,
            "rank": 85
        },
        "chemistry": {
            "marks": 104,
            "percentage": 95.55,
            "rank": 24
        },
        "maths": {
            "marks": 110,
            "percentage": 96.74,
            "rank": 65
        },
        "total": {
            "marks": 354,
            "percentage": 96.28,
            "rank": 68
        }
    },
    {
        "OMRID": 620453246,
        "name": "Bridgett Cook",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 114,
            "percentage": 98.11,
            "rank": 71
        },
        "chemistry": {
            "marks": 110,
            "percentage": 99.93,
            "rank": 49
        },
        "maths": {
            "marks": 101,
            "percentage": 95.37,
            "rank": 10
        },
        "total": {
            "marks": 312,
            "percentage": 99.6,
            "rank": 44
        }
    },
    {
        "OMRID": 907035247,
        "name": "Dennis Bowman",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 108,
            "percentage": 97.89,
            "rank": 46
        },
        "chemistry": {
            "marks": 111,
            "percentage": 98.99,
            "rank": 73
        },
        "maths": {
            "marks": 114,
            "percentage": 96.46,
            "rank": 94
        },
        "total": {
            "marks": 313,
            "percentage": 98.08,
            "rank": 80
        }
    },
    {
        "OMRID": 247688248,
        "name": "Hilary Hamilton",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 100,
            "percentage": 99.89,
            "rank": 93
        },
        "chemistry": {
            "marks": 104,
            "percentage": 95.97,
            "rank": 3
        },
        "maths": {
            "marks": 109,
            "percentage": 96.08,
            "rank": 70
        },
        "total": {
            "marks": 349,
            "percentage": 95.1,
            "rank": 70
        }
    },
    {
        "OMRID": 282595249,
        "name": "Watts Sawyer",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 112,
            "percentage": 97.89,
            "rank": 15
        },
        "chemistry": {
            "marks": 104,
            "percentage": 99.73,
            "rank": 79
        },
        "maths": {
            "marks": 101,
            "percentage": 99.69,
            "rank": 1
        },
        "total": {
            "marks": 332,
            "percentage": 98.41,
            "rank": 68
        }
    },
    {
        "OMRID": 892983250,
        "name": "Lily Collins",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 107,
            "percentage": 98.84,
            "rank": 93
        },
        "chemistry": {
            "marks": 115,
            "percentage": 95.81,
            "rank": 39
        },
        "maths": {
            "marks": 108,
            "percentage": 98.15,
            "rank": 44
        },
        "total": {
            "marks": 330,
            "percentage": 97.57,
            "rank": 3
        }
    },
    {
        "OMRID": 363550251,
        "name": "Hunt Boone",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 114,
            "percentage": 98.24,
            "rank": 46
        },
        "chemistry": {
            "marks": 103,
            "percentage": 99.27,
            "rank": 89
        },
        "maths": {
            "marks": 115,
            "percentage": 98.23,
            "rank": 20
        },
        "total": {
            "marks": 339,
            "percentage": 95.13,
            "rank": 20
        }
    },
    {
        "OMRID": 411271252,
        "name": "Mckenzie Guzman",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 110,
            "percentage": 96.39,
            "rank": 79
        },
        "chemistry": {
            "marks": 120,
            "percentage": 99.93,
            "rank": 31
        },
        "maths": {
            "marks": 115,
            "percentage": 95.53,
            "rank": 60
        },
        "total": {
            "marks": 357,
            "percentage": 98.21,
            "rank": 14
        }
    },
    {
        "OMRID": 181001253,
        "name": "Pearlie Figueroa",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 119,
            "percentage": 98.56,
            "rank": 40
        },
        "chemistry": {
            "marks": 100,
            "percentage": 98.81,
            "rank": 42
        },
        "maths": {
            "marks": 100,
            "percentage": 95.27,
            "rank": 45
        },
        "total": {
            "marks": 327,
            "percentage": 97.52,
            "rank": 75
        }
    },
    {
        "OMRID": 702611254,
        "name": "Dalton Norris",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 101,
            "percentage": 96.53,
            "rank": 74
        },
        "chemistry": {
            "marks": 101,
            "percentage": 99.07,
            "rank": 61
        },
        "maths": {
            "marks": 119,
            "percentage": 96.17,
            "rank": 50
        },
        "total": {
            "marks": 301,
            "percentage": 97.69,
            "rank": 100
        }
    },
    {
        "OMRID": 827058255,
        "name": "Ellis Reilly",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 120,
            "percentage": 98.55,
            "rank": 2
        },
        "chemistry": {
            "marks": 114,
            "percentage": 96.33,
            "rank": 45
        },
        "maths": {
            "marks": 101,
            "percentage": 96.35,
            "rank": 75
        },
        "total": {
            "marks": 355,
            "percentage": 98.39,
            "rank": 60
        }
    },
    {
        "OMRID": 480978256,
        "name": "Lydia Mack",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 100,
            "percentage": 97.37,
            "rank": 58
        },
        "chemistry": {
            "marks": 114,
            "percentage": 96.93,
            "rank": 99
        },
        "maths": {
            "marks": 109,
            "percentage": 96.29,
            "rank": 100
        },
        "total": {
            "marks": 343,
            "percentage": 99.37,
            "rank": 2
        }
    },
    {
        "OMRID": 329343257,
        "name": "Burnett Foley",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 120,
            "percentage": 98.48,
            "rank": 10
        },
        "chemistry": {
            "marks": 113,
            "percentage": 97.81,
            "rank": 57
        },
        "maths": {
            "marks": 120,
            "percentage": 99.51,
            "rank": 62
        },
        "total": {
            "marks": 323,
            "percentage": 97.17,
            "rank": 23
        }
    },
    {
        "OMRID": 139694258,
        "name": "Marissa Bryant",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 102,
            "percentage": 98.98,
            "rank": 100
        },
        "chemistry": {
            "marks": 119,
            "percentage": 99.58,
            "rank": 67
        },
        "maths": {
            "marks": 100,
            "percentage": 99.38,
            "rank": 3
        },
        "total": {
            "marks": 354,
            "percentage": 96.59,
            "rank": 49
        }
    },
    {
        "OMRID": 528700259,
        "name": "Downs Cooke",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 110,
            "percentage": 98.44,
            "rank": 50
        },
        "chemistry": {
            "marks": 100,
            "percentage": 95.23,
            "rank": 33
        },
        "maths": {
            "marks": 111,
            "percentage": 98.96,
            "rank": 43
        },
        "total": {
            "marks": 349,
            "percentage": 96,
            "rank": 88
        }
    },
    {
        "OMRID": 775620260,
        "name": "Josefa Hancock",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 111,
            "percentage": 98.96,
            "rank": 73
        },
        "chemistry": {
            "marks": 101,
            "percentage": 99.5,
            "rank": 66
        },
        "maths": {
            "marks": 115,
            "percentage": 95.16,
            "rank": 21
        },
        "total": {
            "marks": 352,
            "percentage": 95.17,
            "rank": 90
        }
    },
    {
        "OMRID": 968764261,
        "name": "Helga Burnett",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 119,
            "percentage": 95.2,
            "rank": 50
        },
        "chemistry": {
            "marks": 115,
            "percentage": 98.64,
            "rank": 72
        },
        "maths": {
            "marks": 110,
            "percentage": 98.85,
            "rank": 17
        },
        "total": {
            "marks": 328,
            "percentage": 99.93,
            "rank": 47
        }
    },
    {
        "OMRID": 928288262,
        "name": "Pena Lopez",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 103,
            "percentage": 97.42,
            "rank": 24
        },
        "chemistry": {
            "marks": 105,
            "percentage": 99.43,
            "rank": 5
        },
        "maths": {
            "marks": 109,
            "percentage": 97.18,
            "rank": 59
        },
        "total": {
            "marks": 345,
            "percentage": 96.57,
            "rank": 31
        }
    },
    {
        "OMRID": 338492263,
        "name": "Tina Mullen",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 117,
            "percentage": 98.26,
            "rank": 45
        },
        "chemistry": {
            "marks": 109,
            "percentage": 97.75,
            "rank": 1
        },
        "maths": {
            "marks": 112,
            "percentage": 99.61,
            "rank": 41
        },
        "total": {
            "marks": 303,
            "percentage": 99.62,
            "rank": 5
        }
    },
    {
        "OMRID": 800444264,
        "name": "Kristy Dodson",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 108,
            "percentage": 99.86,
            "rank": 65
        },
        "chemistry": {
            "marks": 108,
            "percentage": 98.62,
            "rank": 92
        },
        "maths": {
            "marks": 101,
            "percentage": 99.06,
            "rank": 15
        },
        "total": {
            "marks": 316,
            "percentage": 99.92,
            "rank": 15
        }
    },
    {
        "OMRID": 940422265,
        "name": "Christian Vega",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 119,
            "percentage": 99.87,
            "rank": 11
        },
        "chemistry": {
            "marks": 107,
            "percentage": 95.86,
            "rank": 56
        },
        "maths": {
            "marks": 100,
            "percentage": 99.39,
            "rank": 61
        },
        "total": {
            "marks": 333,
            "percentage": 97.15,
            "rank": 66
        }
    },
    {
        "OMRID": 958161266,
        "name": "Frazier Matthews",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 103,
            "percentage": 97.99,
            "rank": 15
        },
        "chemistry": {
            "marks": 106,
            "percentage": 97.33,
            "rank": 59
        },
        "maths": {
            "marks": 115,
            "percentage": 95.98,
            "rank": 82
        },
        "total": {
            "marks": 349,
            "percentage": 99.21,
            "rank": 86
        }
    },
    {
        "OMRID": 428484267,
        "name": "Eva Davenport",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 117,
            "percentage": 98.59,
            "rank": 45
        },
        "chemistry": {
            "marks": 106,
            "percentage": 95.62,
            "rank": 67
        },
        "maths": {
            "marks": 112,
            "percentage": 95.23,
            "rank": 41
        },
        "total": {
            "marks": 319,
            "percentage": 96.35,
            "rank": 44
        }
    },
    {
        "OMRID": 546353268,
        "name": "Justine Clayton",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 101,
            "percentage": 97.71,
            "rank": 91
        },
        "chemistry": {
            "marks": 100,
            "percentage": 98.7,
            "rank": 34
        },
        "maths": {
            "marks": 115,
            "percentage": 96.16,
            "rank": 93
        },
        "total": {
            "marks": 326,
            "percentage": 95.32,
            "rank": 30
        }
    },
    {
        "OMRID": 977956269,
        "name": "Valarie Baker",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 103,
            "percentage": 96.18,
            "rank": 77
        },
        "chemistry": {
            "marks": 115,
            "percentage": 98.22,
            "rank": 29
        },
        "maths": {
            "marks": 120,
            "percentage": 99.08,
            "rank": 7
        },
        "total": {
            "marks": 339,
            "percentage": 97.61,
            "rank": 22
        }
    },
    {
        "OMRID": 144630270,
        "name": "Aida Montoya",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 118,
            "percentage": 98.82,
            "rank": 55
        },
        "chemistry": {
            "marks": 114,
            "percentage": 98.16,
            "rank": 26
        },
        "maths": {
            "marks": 100,
            "percentage": 95.74,
            "rank": 46
        },
        "total": {
            "marks": 359,
            "percentage": 99.62,
            "rank": 64
        }
    },
    {
        "OMRID": 228905271,
        "name": "Mindy Turner",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 116,
            "percentage": 99.05,
            "rank": 32
        },
        "chemistry": {
            "marks": 118,
            "percentage": 96.61,
            "rank": 35
        },
        "maths": {
            "marks": 105,
            "percentage": 95.92,
            "rank": 43
        },
        "total": {
            "marks": 338,
            "percentage": 98.67,
            "rank": 51
        }
    },
    {
        "OMRID": 295008272,
        "name": "Carmen Meadows",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 117,
            "percentage": 95.18,
            "rank": 90
        },
        "chemistry": {
            "marks": 106,
            "percentage": 99.85,
            "rank": 40
        },
        "maths": {
            "marks": 108,
            "percentage": 97.69,
            "rank": 29
        },
        "total": {
            "marks": 330,
            "percentage": 98.93,
            "rank": 9
        }
    },
    {
        "OMRID": 410533273,
        "name": "Rachelle Foster",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 119,
            "percentage": 98.67,
            "rank": 91
        },
        "chemistry": {
            "marks": 116,
            "percentage": 95.82,
            "rank": 99
        },
        "maths": {
            "marks": 108,
            "percentage": 99.93,
            "rank": 53
        },
        "total": {
            "marks": 303,
            "percentage": 99.67,
            "rank": 14
        }
    },
    {
        "OMRID": 232977274,
        "name": "Estes Cherry",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 100,
            "percentage": 95.31,
            "rank": 40
        },
        "chemistry": {
            "marks": 100,
            "percentage": 95.14,
            "rank": 13
        },
        "maths": {
            "marks": 117,
            "percentage": 95.89,
            "rank": 45
        },
        "total": {
            "marks": 352,
            "percentage": 96.34,
            "rank": 22
        }
    },
    {
        "OMRID": 692482275,
        "name": "Dolly Whitley",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 117,
            "percentage": 98.58,
            "rank": 39
        },
        "chemistry": {
            "marks": 112,
            "percentage": 97.5,
            "rank": 24
        },
        "maths": {
            "marks": 105,
            "percentage": 95.78,
            "rank": 22
        },
        "total": {
            "marks": 357,
            "percentage": 96.28,
            "rank": 98
        }
    },
    {
        "OMRID": 494175276,
        "name": "Reyes Grimes",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 112,
            "percentage": 96.98,
            "rank": 94
        },
        "chemistry": {
            "marks": 100,
            "percentage": 95.82,
            "rank": 7
        },
        "maths": {
            "marks": 107,
            "percentage": 95.73,
            "rank": 50
        },
        "total": {
            "marks": 344,
            "percentage": 99.56,
            "rank": 13
        }
    },
    {
        "OMRID": 634829277,
        "name": "Wade Swanson",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 109,
            "percentage": 96.4,
            "rank": 6
        },
        "chemistry": {
            "marks": 112,
            "percentage": 99.08,
            "rank": 54
        },
        "maths": {
            "marks": 106,
            "percentage": 98.52,
            "rank": 58
        },
        "total": {
            "marks": 302,
            "percentage": 99.59,
            "rank": 84
        }
    },
    {
        "OMRID": 334729278,
        "name": "Aurora Carrillo",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 120,
            "percentage": 96.42,
            "rank": 69
        },
        "chemistry": {
            "marks": 104,
            "percentage": 96.05,
            "rank": 72
        },
        "maths": {
            "marks": 109,
            "percentage": 95.09,
            "rank": 54
        },
        "total": {
            "marks": 309,
            "percentage": 99.54,
            "rank": 10
        }
    },
    {
        "OMRID": 170819279,
        "name": "Susanna Hendricks",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 106,
            "percentage": 97.61,
            "rank": 49
        },
        "chemistry": {
            "marks": 103,
            "percentage": 96.3,
            "rank": 90
        },
        "maths": {
            "marks": 104,
            "percentage": 98.56,
            "rank": 88
        },
        "total": {
            "marks": 328,
            "percentage": 97.53,
            "rank": 37
        }
    },
    {
        "OMRID": 151071280,
        "name": "Clarice Fernandez",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 110,
            "percentage": 97.2,
            "rank": 55
        },
        "chemistry": {
            "marks": 102,
            "percentage": 95.39,
            "rank": 2
        },
        "maths": {
            "marks": 117,
            "percentage": 95.65,
            "rank": 88
        },
        "total": {
            "marks": 340,
            "percentage": 99.89,
            "rank": 9
        }
    },
    {
        "OMRID": 543436281,
        "name": "Kaufman Whitfield",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 112,
            "percentage": 96.11,
            "rank": 3
        },
        "chemistry": {
            "marks": 114,
            "percentage": 99.48,
            "rank": 50
        },
        "maths": {
            "marks": 109,
            "percentage": 95.93,
            "rank": 84
        },
        "total": {
            "marks": 305,
            "percentage": 96.09,
            "rank": 86
        }
    },
    {
        "OMRID": 497272282,
        "name": "Rhoda Perry",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 120,
            "percentage": 96.16,
            "rank": 46
        },
        "chemistry": {
            "marks": 104,
            "percentage": 95.14,
            "rank": 7
        },
        "maths": {
            "marks": 112,
            "percentage": 97.97,
            "rank": 68
        },
        "total": {
            "marks": 300,
            "percentage": 96.87,
            "rank": 60
        }
    },
    {
        "OMRID": 415065283,
        "name": "Ann Rhodes",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 108,
            "percentage": 98.06,
            "rank": 28
        },
        "chemistry": {
            "marks": 115,
            "percentage": 99.74,
            "rank": 34
        },
        "maths": {
            "marks": 111,
            "percentage": 97.88,
            "rank": 11
        },
        "total": {
            "marks": 338,
            "percentage": 99.41,
            "rank": 76
        }
    },
    {
        "OMRID": 713482284,
        "name": "Grimes Stephens",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 102,
            "percentage": 99.5,
            "rank": 81
        },
        "chemistry": {
            "marks": 109,
            "percentage": 99.97,
            "rank": 51
        },
        "maths": {
            "marks": 118,
            "percentage": 99.19,
            "rank": 77
        },
        "total": {
            "marks": 336,
            "percentage": 97.11,
            "rank": 4
        }
    },
    {
        "OMRID": 394469285,
        "name": "Casandra Weaver",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 114,
            "percentage": 99.15,
            "rank": 93
        },
        "chemistry": {
            "marks": 114,
            "percentage": 99.78,
            "rank": 71
        },
        "maths": {
            "marks": 102,
            "percentage": 99.08,
            "rank": 34
        },
        "total": {
            "marks": 312,
            "percentage": 98.21,
            "rank": 78
        }
    },
    {
        "OMRID": 250793286,
        "name": "Elsie Velazquez",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 107,
            "percentage": 98.15,
            "rank": 3
        },
        "chemistry": {
            "marks": 119,
            "percentage": 95.43,
            "rank": 12
        },
        "maths": {
            "marks": 107,
            "percentage": 96.98,
            "rank": 21
        },
        "total": {
            "marks": 306,
            "percentage": 95.71,
            "rank": 36
        }
    },
    {
        "OMRID": 853435287,
        "name": "Louisa England",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 106,
            "percentage": 95.28,
            "rank": 80
        },
        "chemistry": {
            "marks": 101,
            "percentage": 98.75,
            "rank": 95
        },
        "maths": {
            "marks": 100,
            "percentage": 97.86,
            "rank": 91
        },
        "total": {
            "marks": 322,
            "percentage": 97.19,
            "rank": 82
        }
    },
    {
        "OMRID": 444875288,
        "name": "Juliette Morgan",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 107,
            "percentage": 98.21,
            "rank": 66
        },
        "chemistry": {
            "marks": 108,
            "percentage": 98.63,
            "rank": 47
        },
        "maths": {
            "marks": 112,
            "percentage": 96.95,
            "rank": 38
        },
        "total": {
            "marks": 317,
            "percentage": 96.73,
            "rank": 44
        }
    },
    {
        "OMRID": 480384289,
        "name": "Olson Salas",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 109,
            "percentage": 96.12,
            "rank": 40
        },
        "chemistry": {
            "marks": 116,
            "percentage": 96.62,
            "rank": 68
        },
        "maths": {
            "marks": 100,
            "percentage": 97.5,
            "rank": 6
        },
        "total": {
            "marks": 319,
            "percentage": 95.73,
            "rank": 63
        }
    },
    {
        "OMRID": 428644290,
        "name": "Lilian Estes",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 120,
            "percentage": 99.42,
            "rank": 64
        },
        "chemistry": {
            "marks": 113,
            "percentage": 97.47,
            "rank": 85
        },
        "maths": {
            "marks": 108,
            "percentage": 97.28,
            "rank": 59
        },
        "total": {
            "marks": 304,
            "percentage": 98.17,
            "rank": 55
        }
    },
    {
        "OMRID": 274025291,
        "name": "Jannie Riddle",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 102,
            "percentage": 97.76,
            "rank": 81
        },
        "chemistry": {
            "marks": 107,
            "percentage": 96.15,
            "rank": 9
        },
        "maths": {
            "marks": 113,
            "percentage": 97.08,
            "rank": 25
        },
        "total": {
            "marks": 335,
            "percentage": 97.51,
            "rank": 4
        }
    },
    {
        "OMRID": 671504292,
        "name": "Malinda Hayden",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 102,
            "percentage": 98.62,
            "rank": 54
        },
        "chemistry": {
            "marks": 119,
            "percentage": 97.71,
            "rank": 85
        },
        "maths": {
            "marks": 115,
            "percentage": 97.35,
            "rank": 41
        },
        "total": {
            "marks": 302,
            "percentage": 96.55,
            "rank": 52
        }
    },
    {
        "OMRID": 824876293,
        "name": "Polly Wyatt",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 112,
            "percentage": 96.5,
            "rank": 66
        },
        "chemistry": {
            "marks": 111,
            "percentage": 96.47,
            "rank": 14
        },
        "maths": {
            "marks": 107,
            "percentage": 96.58,
            "rank": 49
        },
        "total": {
            "marks": 358,
            "percentage": 95.22,
            "rank": 15
        }
    },
    {
        "OMRID": 992997294,
        "name": "Lester Howard",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 115,
            "percentage": 96.2,
            "rank": 99
        },
        "chemistry": {
            "marks": 120,
            "percentage": 96.72,
            "rank": 78
        },
        "maths": {
            "marks": 115,
            "percentage": 97.63,
            "rank": 93
        },
        "total": {
            "marks": 318,
            "percentage": 95.5,
            "rank": 43
        }
    },
    {
        "OMRID": 936718295,
        "name": "Stein Shaffer",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 103,
            "percentage": 99.05,
            "rank": 67
        },
        "chemistry": {
            "marks": 112,
            "percentage": 97.38,
            "rank": 54
        },
        "maths": {
            "marks": 103,
            "percentage": 99.78,
            "rank": 3
        },
        "total": {
            "marks": 324,
            "percentage": 97.65,
            "rank": 3
        }
    },
    {
        "OMRID": 167064296,
        "name": "Donaldson Carr",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 109,
            "percentage": 97.71,
            "rank": 70
        },
        "chemistry": {
            "marks": 117,
            "percentage": 97.95,
            "rank": 54
        },
        "maths": {
            "marks": 114,
            "percentage": 99.8,
            "rank": 28
        },
        "total": {
            "marks": 327,
            "percentage": 97.31,
            "rank": 42
        }
    },
    {
        "OMRID": 219571297,
        "name": "Pauline Moreno",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 101,
            "percentage": 95.48,
            "rank": 13
        },
        "chemistry": {
            "marks": 109,
            "percentage": 97.37,
            "rank": 29
        },
        "maths": {
            "marks": 113,
            "percentage": 99.28,
            "rank": 49
        },
        "total": {
            "marks": 320,
            "percentage": 98.87,
            "rank": 57
        }
    },
    {
        "OMRID": 518123298,
        "name": "Owen Zamora",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 112,
            "percentage": 96.78,
            "rank": 2
        },
        "chemistry": {
            "marks": 103,
            "percentage": 98.26,
            "rank": 14
        },
        "maths": {
            "marks": 105,
            "percentage": 97.61,
            "rank": 65
        },
        "total": {
            "marks": 318,
            "percentage": 95.12,
            "rank": 37
        }
    },
    {
        "OMRID": 670728299,
        "name": "Solis Mcintosh",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 110,
            "percentage": 95.84,
            "rank": 10
        },
        "chemistry": {
            "marks": 107,
            "percentage": 96.23,
            "rank": 17
        },
        "maths": {
            "marks": 105,
            "percentage": 99.79,
            "rank": 78
        },
        "total": {
            "marks": 326,
            "percentage": 98.52,
            "rank": 52
        }
    },
    {
        "OMRID": 776216300,
        "name": "Milagros Hurst",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 108,
            "percentage": 99.59,
            "rank": 100
        },
        "chemistry": {
            "marks": 106,
            "percentage": 97.5,
            "rank": 53
        },
        "maths": {
            "marks": 100,
            "percentage": 96.59,
            "rank": 10
        },
        "total": {
            "marks": 348,
            "percentage": 98.02,
            "rank": 37
        }
    },
    {
        "OMRID": 856488301,
        "name": "Schwartz Harrison",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 118,
            "percentage": 98.54,
            "rank": 30
        },
        "chemistry": {
            "marks": 113,
            "percentage": 95.52,
            "rank": 23
        },
        "maths": {
            "marks": 114,
            "percentage": 99.48,
            "rank": 93
        },
        "total": {
            "marks": 337,
            "percentage": 97.74,
            "rank": 92
        }
    },
    {
        "OMRID": 898693302,
        "name": "Sims Paul",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 119,
            "percentage": 96.52,
            "rank": 95
        },
        "chemistry": {
            "marks": 102,
            "percentage": 99.46,
            "rank": 88
        },
        "maths": {
            "marks": 106,
            "percentage": 97.27,
            "rank": 50
        },
        "total": {
            "marks": 357,
            "percentage": 99.76,
            "rank": 57
        }
    },
    {
        "OMRID": 865621303,
        "name": "Campos Sellers",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 113,
            "percentage": 95.79,
            "rank": 60
        },
        "chemistry": {
            "marks": 107,
            "percentage": 96.5,
            "rank": 19
        },
        "maths": {
            "marks": 109,
            "percentage": 99.76,
            "rank": 95
        },
        "total": {
            "marks": 304,
            "percentage": 99.27,
            "rank": 86
        }
    },
    {
        "OMRID": 479734304,
        "name": "Wheeler Chase",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 102,
            "percentage": 97.31,
            "rank": 52
        },
        "chemistry": {
            "marks": 109,
            "percentage": 95.79,
            "rank": 90
        },
        "maths": {
            "marks": 115,
            "percentage": 98.47,
            "rank": 61
        },
        "total": {
            "marks": 327,
            "percentage": 98.15,
            "rank": 82
        }
    },
    {
        "OMRID": 274852305,
        "name": "Roy Oneill",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 101,
            "percentage": 99.16,
            "rank": 7
        },
        "chemistry": {
            "marks": 120,
            "percentage": 97.27,
            "rank": 10
        },
        "maths": {
            "marks": 101,
            "percentage": 99.55,
            "rank": 14
        },
        "total": {
            "marks": 355,
            "percentage": 95.72,
            "rank": 71
        }
    },
    {
        "OMRID": 696741306,
        "name": "Hannah Wiley",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 108,
            "percentage": 95.47,
            "rank": 8
        },
        "chemistry": {
            "marks": 113,
            "percentage": 99.04,
            "rank": 16
        },
        "maths": {
            "marks": 113,
            "percentage": 97.57,
            "rank": 82
        },
        "total": {
            "marks": 301,
            "percentage": 96.03,
            "rank": 15
        }
    },
    {
        "OMRID": 802434307,
        "name": "Deloris Short",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 109,
            "percentage": 97.55,
            "rank": 83
        },
        "chemistry": {
            "marks": 112,
            "percentage": 96.81,
            "rank": 96
        },
        "maths": {
            "marks": 116,
            "percentage": 97.63,
            "rank": 42
        },
        "total": {
            "marks": 346,
            "percentage": 98.22,
            "rank": 59
        }
    },
    {
        "OMRID": 780332308,
        "name": "Dena Love",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 113,
            "percentage": 95.06,
            "rank": 83
        },
        "chemistry": {
            "marks": 107,
            "percentage": 96.19,
            "rank": 78
        },
        "maths": {
            "marks": 101,
            "percentage": 99.93,
            "rank": 54
        },
        "total": {
            "marks": 353,
            "percentage": 97.72,
            "rank": 3
        }
    },
    {
        "OMRID": 306219309,
        "name": "Jodi Vaughn",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 109,
            "percentage": 97.39,
            "rank": 72
        },
        "chemistry": {
            "marks": 101,
            "percentage": 98.88,
            "rank": 14
        },
        "maths": {
            "marks": 112,
            "percentage": 95.27,
            "rank": 39
        },
        "total": {
            "marks": 358,
            "percentage": 99.06,
            "rank": 30
        }
    },
    {
        "OMRID": 543023310,
        "name": "Schmidt Pollard",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 102,
            "percentage": 95.8,
            "rank": 30
        },
        "chemistry": {
            "marks": 105,
            "percentage": 99.6,
            "rank": 6
        },
        "maths": {
            "marks": 119,
            "percentage": 96.95,
            "rank": 35
        },
        "total": {
            "marks": 341,
            "percentage": 95.34,
            "rank": 67
        }
    },
    {
        "OMRID": 272097311,
        "name": "Rena Martinez",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 104,
            "percentage": 95.01,
            "rank": 27
        },
        "chemistry": {
            "marks": 107,
            "percentage": 98.5,
            "rank": 96
        },
        "maths": {
            "marks": 115,
            "percentage": 97.11,
            "rank": 4
        },
        "total": {
            "marks": 357,
            "percentage": 99.53,
            "rank": 77
        }
    },
    {
        "OMRID": 742244312,
        "name": "Lupe Solomon",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 104,
            "percentage": 98.18,
            "rank": 35
        },
        "chemistry": {
            "marks": 115,
            "percentage": 96.45,
            "rank": 44
        },
        "maths": {
            "marks": 116,
            "percentage": 95.8,
            "rank": 59
        },
        "total": {
            "marks": 353,
            "percentage": 97.58,
            "rank": 69
        }
    },
    {
        "OMRID": 438856313,
        "name": "Lenore Bradford",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 107,
            "percentage": 96.94,
            "rank": 7
        },
        "chemistry": {
            "marks": 101,
            "percentage": 96.1,
            "rank": 54
        },
        "maths": {
            "marks": 102,
            "percentage": 97.91,
            "rank": 88
        },
        "total": {
            "marks": 307,
            "percentage": 95.73,
            "rank": 1
        }
    },
    {
        "OMRID": 947347314,
        "name": "Sheena Stanley",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 118,
            "percentage": 99.68,
            "rank": 68
        },
        "chemistry": {
            "marks": 113,
            "percentage": 98.76,
            "rank": 1
        },
        "maths": {
            "marks": 118,
            "percentage": 96.64,
            "rank": 26
        },
        "total": {
            "marks": 349,
            "percentage": 95.22,
            "rank": 36
        }
    },
    {
        "OMRID": 460954315,
        "name": "Everett Cervantes",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 110,
            "percentage": 96.58,
            "rank": 40
        },
        "chemistry": {
            "marks": 116,
            "percentage": 95.23,
            "rank": 67
        },
        "maths": {
            "marks": 116,
            "percentage": 99.73,
            "rank": 67
        },
        "total": {
            "marks": 341,
            "percentage": 95.11,
            "rank": 36
        }
    },
    {
        "OMRID": 451973316,
        "name": "Garner Simon",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 112,
            "percentage": 98.39,
            "rank": 13
        },
        "chemistry": {
            "marks": 101,
            "percentage": 96.14,
            "rank": 45
        },
        "maths": {
            "marks": 106,
            "percentage": 95.56,
            "rank": 40
        },
        "total": {
            "marks": 348,
            "percentage": 97.66,
            "rank": 16
        }
    },
    {
        "OMRID": 393869317,
        "name": "Shanna Riley",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 111,
            "percentage": 99.25,
            "rank": 15
        },
        "chemistry": {
            "marks": 106,
            "percentage": 96.32,
            "rank": 78
        },
        "maths": {
            "marks": 110,
            "percentage": 97.24,
            "rank": 76
        },
        "total": {
            "marks": 348,
            "percentage": 95.06,
            "rank": 69
        }
    },
    {
        "OMRID": 276963318,
        "name": "Knox Poole",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 118,
            "percentage": 95.47,
            "rank": 69
        },
        "chemistry": {
            "marks": 110,
            "percentage": 96.2,
            "rank": 69
        },
        "maths": {
            "marks": 101,
            "percentage": 98.61,
            "rank": 18
        },
        "total": {
            "marks": 346,
            "percentage": 98.33,
            "rank": 22
        }
    },
    {
        "OMRID": 948557319,
        "name": "Kendra Houston",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 107,
            "percentage": 96.69,
            "rank": 43
        },
        "chemistry": {
            "marks": 105,
            "percentage": 96.93,
            "rank": 45
        },
        "maths": {
            "marks": 105,
            "percentage": 95,
            "rank": 89
        },
        "total": {
            "marks": 334,
            "percentage": 96.9,
            "rank": 95
        }
    },
    {
        "OMRID": 186676320,
        "name": "Lucy Carey",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 105,
            "percentage": 97.91,
            "rank": 36
        },
        "chemistry": {
            "marks": 111,
            "percentage": 96.07,
            "rank": 9
        },
        "maths": {
            "marks": 108,
            "percentage": 95.98,
            "rank": 37
        },
        "total": {
            "marks": 352,
            "percentage": 96.72,
            "rank": 27
        }
    },
    {
        "OMRID": 691393321,
        "name": "Cornelia Rogers",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 112,
            "percentage": 96.81,
            "rank": 33
        },
        "chemistry": {
            "marks": 106,
            "percentage": 97.82,
            "rank": 3
        },
        "maths": {
            "marks": 103,
            "percentage": 95.91,
            "rank": 56
        },
        "total": {
            "marks": 314,
            "percentage": 98.3,
            "rank": 3
        }
    },
    {
        "OMRID": 505070322,
        "name": "Howe Mcclure",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 112,
            "percentage": 98.85,
            "rank": 18
        },
        "chemistry": {
            "marks": 120,
            "percentage": 95.65,
            "rank": 92
        },
        "maths": {
            "marks": 100,
            "percentage": 96.41,
            "rank": 19
        },
        "total": {
            "marks": 334,
            "percentage": 98.24,
            "rank": 42
        }
    },
    {
        "OMRID": 100220323,
        "name": "Cantrell Fuentes",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 101,
            "percentage": 97.26,
            "rank": 48
        },
        "chemistry": {
            "marks": 101,
            "percentage": 95.7,
            "rank": 20
        },
        "maths": {
            "marks": 103,
            "percentage": 99.5,
            "rank": 14
        },
        "total": {
            "marks": 322,
            "percentage": 95.84,
            "rank": 49
        }
    },
    {
        "OMRID": 365310324,
        "name": "Lynne Farley",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 118,
            "percentage": 97.16,
            "rank": 6
        },
        "chemistry": {
            "marks": 100,
            "percentage": 97.21,
            "rank": 26
        },
        "maths": {
            "marks": 117,
            "percentage": 96.81,
            "rank": 52
        },
        "total": {
            "marks": 337,
            "percentage": 95.95,
            "rank": 97
        }
    },
    {
        "OMRID": 943476325,
        "name": "Burt Mcgee",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 111,
            "percentage": 99.26,
            "rank": 95
        },
        "chemistry": {
            "marks": 116,
            "percentage": 95.33,
            "rank": 91
        },
        "maths": {
            "marks": 109,
            "percentage": 99.17,
            "rank": 85
        },
        "total": {
            "marks": 319,
            "percentage": 97.97,
            "rank": 86
        }
    },
    {
        "OMRID": 765962326,
        "name": "Gomez Tillman",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 108,
            "percentage": 96.43,
            "rank": 77
        },
        "chemistry": {
            "marks": 103,
            "percentage": 95.59,
            "rank": 66
        },
        "maths": {
            "marks": 102,
            "percentage": 96.9,
            "rank": 42
        },
        "total": {
            "marks": 346,
            "percentage": 96.78,
            "rank": 81
        }
    },
    {
        "OMRID": 884345327,
        "name": "Tamra Wood",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 113,
            "percentage": 97.41,
            "rank": 73
        },
        "chemistry": {
            "marks": 109,
            "percentage": 98.96,
            "rank": 31
        },
        "maths": {
            "marks": 116,
            "percentage": 97.77,
            "rank": 60
        },
        "total": {
            "marks": 339,
            "percentage": 97.47,
            "rank": 52
        }
    },
    {
        "OMRID": 926263328,
        "name": "Sanders Morrow",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 101,
            "percentage": 95.27,
            "rank": 5
        },
        "chemistry": {
            "marks": 102,
            "percentage": 99.07,
            "rank": 26
        },
        "maths": {
            "marks": 105,
            "percentage": 96.64,
            "rank": 19
        },
        "total": {
            "marks": 333,
            "percentage": 97.91,
            "rank": 72
        }
    },
    {
        "OMRID": 430060329,
        "name": "Jones Ayers",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 101,
            "percentage": 98.44,
            "rank": 90
        },
        "chemistry": {
            "marks": 110,
            "percentage": 96.38,
            "rank": 18
        },
        "maths": {
            "marks": 115,
            "percentage": 96.2,
            "rank": 68
        },
        "total": {
            "marks": 353,
            "percentage": 98.16,
            "rank": 77
        }
    },
    {
        "OMRID": 168819330,
        "name": "Christian Mclean",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 102,
            "percentage": 98.62,
            "rank": 46
        },
        "chemistry": {
            "marks": 100,
            "percentage": 95.21,
            "rank": 49
        },
        "maths": {
            "marks": 110,
            "percentage": 99,
            "rank": 77
        },
        "total": {
            "marks": 317,
            "percentage": 95.2,
            "rank": 17
        }
    },
    {
        "OMRID": 547290331,
        "name": "Ramos Malone",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 116,
            "percentage": 97.09,
            "rank": 32
        },
        "chemistry": {
            "marks": 101,
            "percentage": 95.34,
            "rank": 92
        },
        "maths": {
            "marks": 119,
            "percentage": 97.02,
            "rank": 71
        },
        "total": {
            "marks": 307,
            "percentage": 96.12,
            "rank": 66
        }
    },
    {
        "OMRID": 739638332,
        "name": "Gladys Slater",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 111,
            "percentage": 98.33,
            "rank": 4
        },
        "chemistry": {
            "marks": 106,
            "percentage": 95.47,
            "rank": 61
        },
        "maths": {
            "marks": 113,
            "percentage": 98.31,
            "rank": 60
        },
        "total": {
            "marks": 313,
            "percentage": 95.01,
            "rank": 63
        }
    },
    {
        "OMRID": 643260333,
        "name": "Luisa Hines",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 100,
            "percentage": 98.65,
            "rank": 20
        },
        "chemistry": {
            "marks": 108,
            "percentage": 99.04,
            "rank": 31
        },
        "maths": {
            "marks": 108,
            "percentage": 97.98,
            "rank": 52
        },
        "total": {
            "marks": 333,
            "percentage": 98.78,
            "rank": 59
        }
    },
    {
        "OMRID": 513628334,
        "name": "Winnie Chang",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 116,
            "percentage": 98.14,
            "rank": 6
        },
        "chemistry": {
            "marks": 101,
            "percentage": 96.83,
            "rank": 47
        },
        "maths": {
            "marks": 114,
            "percentage": 96.93,
            "rank": 80
        },
        "total": {
            "marks": 338,
            "percentage": 97.69,
            "rank": 39
        }
    },
    {
        "OMRID": 130473335,
        "name": "Brenda Vazquez",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 114,
            "percentage": 98.01,
            "rank": 91
        },
        "chemistry": {
            "marks": 109,
            "percentage": 99.07,
            "rank": 7
        },
        "maths": {
            "marks": 106,
            "percentage": 97.62,
            "rank": 50
        },
        "total": {
            "marks": 349,
            "percentage": 99.39,
            "rank": 64
        }
    },
    {
        "OMRID": 309619336,
        "name": "Turner Mccormick",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 109,
            "percentage": 99.56,
            "rank": 99
        },
        "chemistry": {
            "marks": 102,
            "percentage": 97.53,
            "rank": 78
        },
        "maths": {
            "marks": 118,
            "percentage": 95.24,
            "rank": 28
        },
        "total": {
            "marks": 342,
            "percentage": 99.1,
            "rank": 83
        }
    },
    {
        "OMRID": 577612337,
        "name": "Maxwell Hopkins",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 114,
            "percentage": 98.28,
            "rank": 8
        },
        "chemistry": {
            "marks": 100,
            "percentage": 99.45,
            "rank": 32
        },
        "maths": {
            "marks": 110,
            "percentage": 95.65,
            "rank": 46
        },
        "total": {
            "marks": 324,
            "percentage": 96.04,
            "rank": 84
        }
    },
    {
        "OMRID": 323582338,
        "name": "Louella Parker",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 115,
            "percentage": 95.57,
            "rank": 1
        },
        "chemistry": {
            "marks": 110,
            "percentage": 95.37,
            "rank": 87
        },
        "maths": {
            "marks": 102,
            "percentage": 98.18,
            "rank": 70
        },
        "total": {
            "marks": 300,
            "percentage": 96.57,
            "rank": 1
        }
    },
    {
        "OMRID": 573331339,
        "name": "Valentine Horn",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 108,
            "percentage": 96.65,
            "rank": 10
        },
        "chemistry": {
            "marks": 115,
            "percentage": 98.5,
            "rank": 97
        },
        "maths": {
            "marks": 109,
            "percentage": 96.89,
            "rank": 28
        },
        "total": {
            "marks": 358,
            "percentage": 97.73,
            "rank": 22
        }
    },
    {
        "OMRID": 773431340,
        "name": "Chaney Sykes",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 103,
            "percentage": 95.48,
            "rank": 27
        },
        "chemistry": {
            "marks": 102,
            "percentage": 96.72,
            "rank": 70
        },
        "maths": {
            "marks": 102,
            "percentage": 95.46,
            "rank": 36
        },
        "total": {
            "marks": 357,
            "percentage": 99.52,
            "rank": 42
        }
    },
    {
        "OMRID": 845818341,
        "name": "Orr Blevins",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 109,
            "percentage": 99.88,
            "rank": 8
        },
        "chemistry": {
            "marks": 105,
            "percentage": 99.08,
            "rank": 31
        },
        "maths": {
            "marks": 105,
            "percentage": 95.31,
            "rank": 28
        },
        "total": {
            "marks": 356,
            "percentage": 97.4,
            "rank": 56
        }
    },
    {
        "OMRID": 276134342,
        "name": "Sharron Cobb",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 108,
            "percentage": 97.86,
            "rank": 47
        },
        "chemistry": {
            "marks": 103,
            "percentage": 99.72,
            "rank": 89
        },
        "maths": {
            "marks": 112,
            "percentage": 96.22,
            "rank": 80
        },
        "total": {
            "marks": 317,
            "percentage": 97.63,
            "rank": 44
        }
    },
    {
        "OMRID": 185558343,
        "name": "Marcella Wilson",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 100,
            "percentage": 96.79,
            "rank": 86
        },
        "chemistry": {
            "marks": 108,
            "percentage": 96.28,
            "rank": 28
        },
        "maths": {
            "marks": 118,
            "percentage": 99.57,
            "rank": 98
        },
        "total": {
            "marks": 350,
            "percentage": 95.34,
            "rank": 82
        }
    },
    {
        "OMRID": 368547344,
        "name": "Terrell Dillon",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 111,
            "percentage": 97.62,
            "rank": 45
        },
        "chemistry": {
            "marks": 101,
            "percentage": 97.96,
            "rank": 77
        },
        "maths": {
            "marks": 111,
            "percentage": 96.83,
            "rank": 63
        },
        "total": {
            "marks": 341,
            "percentage": 98.44,
            "rank": 9
        }
    },
    {
        "OMRID": 970731345,
        "name": "Neva Mcmillan",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 115,
            "percentage": 95.96,
            "rank": 79
        },
        "chemistry": {
            "marks": 116,
            "percentage": 98.63,
            "rank": 9
        },
        "maths": {
            "marks": 117,
            "percentage": 95.38,
            "rank": 12
        },
        "total": {
            "marks": 331,
            "percentage": 99.31,
            "rank": 17
        }
    },
    {
        "OMRID": 734892346,
        "name": "Claudette Bond",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 120,
            "percentage": 95.84,
            "rank": 24
        },
        "chemistry": {
            "marks": 120,
            "percentage": 95.32,
            "rank": 99
        },
        "maths": {
            "marks": 106,
            "percentage": 96.88,
            "rank": 90
        },
        "total": {
            "marks": 324,
            "percentage": 99.52,
            "rank": 72
        }
    },
    {
        "OMRID": 771340347,
        "name": "Mcclain Odonnell",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 116,
            "percentage": 96.09,
            "rank": 75
        },
        "chemistry": {
            "marks": 114,
            "percentage": 96.73,
            "rank": 44
        },
        "maths": {
            "marks": 109,
            "percentage": 99.35,
            "rank": 96
        },
        "total": {
            "marks": 323,
            "percentage": 98.77,
            "rank": 52
        }
    },
    {
        "OMRID": 664506348,
        "name": "Steele Cantu",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 111,
            "percentage": 96.83,
            "rank": 57
        },
        "chemistry": {
            "marks": 104,
            "percentage": 99.73,
            "rank": 34
        },
        "maths": {
            "marks": 120,
            "percentage": 95.76,
            "rank": 44
        },
        "total": {
            "marks": 350,
            "percentage": 96.62,
            "rank": 47
        }
    },
    {
        "OMRID": 409004349,
        "name": "Mcmillan Fields",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 120,
            "percentage": 96.2,
            "rank": 88
        },
        "chemistry": {
            "marks": 105,
            "percentage": 98.24,
            "rank": 23
        },
        "maths": {
            "marks": 111,
            "percentage": 98.91,
            "rank": 46
        },
        "total": {
            "marks": 344,
            "percentage": 95.66,
            "rank": 61
        }
    },
    {
        "OMRID": 468445350,
        "name": "Earlene Dudley",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 112,
            "percentage": 98.51,
            "rank": 42
        },
        "chemistry": {
            "marks": 103,
            "percentage": 96.82,
            "rank": 91
        },
        "maths": {
            "marks": 113,
            "percentage": 99.98,
            "rank": 35
        },
        "total": {
            "marks": 331,
            "percentage": 99.02,
            "rank": 30
        }
    },
    {
        "OMRID": 828439351,
        "name": "Day Acosta",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 120,
            "percentage": 99.52,
            "rank": 87
        },
        "chemistry": {
            "marks": 119,
            "percentage": 97.13,
            "rank": 76
        },
        "maths": {
            "marks": 118,
            "percentage": 99.5,
            "rank": 83
        },
        "total": {
            "marks": 332,
            "percentage": 97.88,
            "rank": 98
        }
    },
    {
        "OMRID": 808298352,
        "name": "Raymond Reese",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 119,
            "percentage": 98.4,
            "rank": 55
        },
        "chemistry": {
            "marks": 118,
            "percentage": 98.14,
            "rank": 13
        },
        "maths": {
            "marks": 107,
            "percentage": 98.58,
            "rank": 34
        },
        "total": {
            "marks": 335,
            "percentage": 98.16,
            "rank": 52
        }
    },
    {
        "OMRID": 140899353,
        "name": "Krista Reyes",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 116,
            "percentage": 96.24,
            "rank": 92
        },
        "chemistry": {
            "marks": 102,
            "percentage": 97.96,
            "rank": 35
        },
        "maths": {
            "marks": 102,
            "percentage": 97.9,
            "rank": 24
        },
        "total": {
            "marks": 352,
            "percentage": 96.77,
            "rank": 19
        }
    },
    {
        "OMRID": 898042354,
        "name": "Jo Hood",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 100,
            "percentage": 95.23,
            "rank": 70
        },
        "chemistry": {
            "marks": 114,
            "percentage": 98.68,
            "rank": 80
        },
        "maths": {
            "marks": 108,
            "percentage": 98.02,
            "rank": 50
        },
        "total": {
            "marks": 329,
            "percentage": 96.88,
            "rank": 5
        }
    },
    {
        "OMRID": 720890355,
        "name": "Meagan Lowery",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 113,
            "percentage": 99.21,
            "rank": 59
        },
        "chemistry": {
            "marks": 120,
            "percentage": 97.28,
            "rank": 38
        },
        "maths": {
            "marks": 109,
            "percentage": 98.33,
            "rank": 32
        },
        "total": {
            "marks": 350,
            "percentage": 95.81,
            "rank": 84
        }
    },
    {
        "OMRID": 384585356,
        "name": "Valenzuela Tate",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 100,
            "percentage": 96.66,
            "rank": 71
        },
        "chemistry": {
            "marks": 108,
            "percentage": 98.96,
            "rank": 13
        },
        "maths": {
            "marks": 114,
            "percentage": 98.81,
            "rank": 70
        },
        "total": {
            "marks": 330,
            "percentage": 95.51,
            "rank": 64
        }
    },
    {
        "OMRID": 777516357,
        "name": "Silvia Russo",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 120,
            "percentage": 95.35,
            "rank": 40
        },
        "chemistry": {
            "marks": 101,
            "percentage": 95.45,
            "rank": 29
        },
        "maths": {
            "marks": 115,
            "percentage": 97.17,
            "rank": 3
        },
        "total": {
            "marks": 334,
            "percentage": 95.54,
            "rank": 79
        }
    },
    {
        "OMRID": 988282358,
        "name": "Sheryl Levy",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 103,
            "percentage": 96.93,
            "rank": 60
        },
        "chemistry": {
            "marks": 107,
            "percentage": 97.11,
            "rank": 9
        },
        "maths": {
            "marks": 102,
            "percentage": 97.02,
            "rank": 56
        },
        "total": {
            "marks": 306,
            "percentage": 99.57,
            "rank": 25
        }
    },
    {
        "OMRID": 423210359,
        "name": "Lenora Cleveland",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 101,
            "percentage": 97.44,
            "rank": 44
        },
        "chemistry": {
            "marks": 116,
            "percentage": 95.07,
            "rank": 38
        },
        "maths": {
            "marks": 103,
            "percentage": 98.52,
            "rank": 51
        },
        "total": {
            "marks": 356,
            "percentage": 95.09,
            "rank": 81
        }
    },
    {
        "OMRID": 417694360,
        "name": "Mccarty Thornton",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 115,
            "percentage": 96.38,
            "rank": 85
        },
        "chemistry": {
            "marks": 100,
            "percentage": 96.26,
            "rank": 49
        },
        "maths": {
            "marks": 117,
            "percentage": 99.97,
            "rank": 96
        },
        "total": {
            "marks": 353,
            "percentage": 96.51,
            "rank": 57
        }
    },
    {
        "OMRID": 951550361,
        "name": "Ortega French",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 102,
            "percentage": 97.59,
            "rank": 60
        },
        "chemistry": {
            "marks": 104,
            "percentage": 96.81,
            "rank": 19
        },
        "maths": {
            "marks": 117,
            "percentage": 96.8,
            "rank": 8
        },
        "total": {
            "marks": 322,
            "percentage": 95.09,
            "rank": 63
        }
    },
    {
        "OMRID": 236986362,
        "name": "Jacqueline Austin",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 118,
            "percentage": 95.68,
            "rank": 21
        },
        "chemistry": {
            "marks": 109,
            "percentage": 97.48,
            "rank": 22
        },
        "maths": {
            "marks": 120,
            "percentage": 97.21,
            "rank": 57
        },
        "total": {
            "marks": 346,
            "percentage": 96.55,
            "rank": 37
        }
    },
    {
        "OMRID": 636714363,
        "name": "Workman Travis",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 115,
            "percentage": 98.73,
            "rank": 81
        },
        "chemistry": {
            "marks": 104,
            "percentage": 95.99,
            "rank": 21
        },
        "maths": {
            "marks": 120,
            "percentage": 96.51,
            "rank": 47
        },
        "total": {
            "marks": 343,
            "percentage": 98.68,
            "rank": 41
        }
    },
    {
        "OMRID": 608077364,
        "name": "Meadows Johnson",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 117,
            "percentage": 97.27,
            "rank": 31
        },
        "chemistry": {
            "marks": 101,
            "percentage": 99.25,
            "rank": 31
        },
        "maths": {
            "marks": 107,
            "percentage": 96.72,
            "rank": 96
        },
        "total": {
            "marks": 325,
            "percentage": 98.49,
            "rank": 18
        }
    },
    {
        "OMRID": 324318365,
        "name": "Luna Fischer",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 105,
            "percentage": 95.99,
            "rank": 10
        },
        "chemistry": {
            "marks": 111,
            "percentage": 99.6,
            "rank": 26
        },
        "maths": {
            "marks": 104,
            "percentage": 97.56,
            "rank": 21
        },
        "total": {
            "marks": 328,
            "percentage": 97.19,
            "rank": 67
        }
    },
    {
        "OMRID": 247883366,
        "name": "Burke Crawford",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 104,
            "percentage": 96.92,
            "rank": 55
        },
        "chemistry": {
            "marks": 119,
            "percentage": 99.34,
            "rank": 63
        },
        "maths": {
            "marks": 101,
            "percentage": 98.21,
            "rank": 42
        },
        "total": {
            "marks": 300,
            "percentage": 98.14,
            "rank": 87
        }
    },
    {
        "OMRID": 732421367,
        "name": "Graciela Washington",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 100,
            "percentage": 97.96,
            "rank": 80
        },
        "chemistry": {
            "marks": 111,
            "percentage": 98.15,
            "rank": 17
        },
        "maths": {
            "marks": 106,
            "percentage": 95.27,
            "rank": 3
        },
        "total": {
            "marks": 349,
            "percentage": 95.06,
            "rank": 19
        }
    },
    {
        "OMRID": 290909368,
        "name": "Reilly Bailey",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 119,
            "percentage": 97.58,
            "rank": 17
        },
        "chemistry": {
            "marks": 103,
            "percentage": 96.59,
            "rank": 27
        },
        "maths": {
            "marks": 112,
            "percentage": 97.79,
            "rank": 4
        },
        "total": {
            "marks": 304,
            "percentage": 95.41,
            "rank": 89
        }
    },
    {
        "OMRID": 570614369,
        "name": "Joan Hull",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 109,
            "percentage": 95.31,
            "rank": 81
        },
        "chemistry": {
            "marks": 119,
            "percentage": 96.11,
            "rank": 70
        },
        "maths": {
            "marks": 103,
            "percentage": 97.22,
            "rank": 46
        },
        "total": {
            "marks": 330,
            "percentage": 99.58,
            "rank": 8
        }
    },
    {
        "OMRID": 611752370,
        "name": "Monroe Hansen",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 112,
            "percentage": 96.43,
            "rank": 86
        },
        "chemistry": {
            "marks": 105,
            "percentage": 96.25,
            "rank": 72
        },
        "maths": {
            "marks": 111,
            "percentage": 95.7,
            "rank": 38
        },
        "total": {
            "marks": 302,
            "percentage": 99.17,
            "rank": 31
        }
    },
    {
        "OMRID": 939727371,
        "name": "Nadine Dean",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 107,
            "percentage": 97.98,
            "rank": 9
        },
        "chemistry": {
            "marks": 112,
            "percentage": 95.25,
            "rank": 85
        },
        "maths": {
            "marks": 100,
            "percentage": 95.48,
            "rank": 69
        },
        "total": {
            "marks": 355,
            "percentage": 98.78,
            "rank": 49
        }
    },
    {
        "OMRID": 262070372,
        "name": "Tisha Anderson",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 107,
            "percentage": 97.41,
            "rank": 57
        },
        "chemistry": {
            "marks": 114,
            "percentage": 97.92,
            "rank": 68
        },
        "maths": {
            "marks": 119,
            "percentage": 97.15,
            "rank": 96
        },
        "total": {
            "marks": 343,
            "percentage": 98.72,
            "rank": 96
        }
    },
    {
        "OMRID": 459524373,
        "name": "Tammi Mcfarland",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 110,
            "percentage": 99.78,
            "rank": 89
        },
        "chemistry": {
            "marks": 110,
            "percentage": 95.9,
            "rank": 1
        },
        "maths": {
            "marks": 111,
            "percentage": 98.25,
            "rank": 12
        },
        "total": {
            "marks": 320,
            "percentage": 99.55,
            "rank": 3
        }
    },
    {
        "OMRID": 764810374,
        "name": "Kim Hobbs",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 118,
            "percentage": 99.31,
            "rank": 64
        },
        "chemistry": {
            "marks": 110,
            "percentage": 99.13,
            "rank": 34
        },
        "maths": {
            "marks": 107,
            "percentage": 99.77,
            "rank": 46
        },
        "total": {
            "marks": 309,
            "percentage": 99.99,
            "rank": 32
        }
    },
    {
        "OMRID": 573848375,
        "name": "Moreno Cole",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 102,
            "percentage": 95.45,
            "rank": 66
        },
        "chemistry": {
            "marks": 115,
            "percentage": 98.48,
            "rank": 92
        },
        "maths": {
            "marks": 107,
            "percentage": 98.16,
            "rank": 78
        },
        "total": {
            "marks": 354,
            "percentage": 95.59,
            "rank": 12
        }
    },
    {
        "OMRID": 539316376,
        "name": "Tanisha Cross",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 105,
            "percentage": 99.69,
            "rank": 36
        },
        "chemistry": {
            "marks": 100,
            "percentage": 96.75,
            "rank": 45
        },
        "maths": {
            "marks": 120,
            "percentage": 95.83,
            "rank": 47
        },
        "total": {
            "marks": 316,
            "percentage": 99.44,
            "rank": 20
        }
    },
    {
        "OMRID": 540821377,
        "name": "Spears May",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 103,
            "percentage": 99.75,
            "rank": 81
        },
        "chemistry": {
            "marks": 107,
            "percentage": 99.13,
            "rank": 80
        },
        "maths": {
            "marks": 102,
            "percentage": 95.57,
            "rank": 22
        },
        "total": {
            "marks": 321,
            "percentage": 96.44,
            "rank": 21
        }
    },
    {
        "OMRID": 825920378,
        "name": "Beatriz Mcknight",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 116,
            "percentage": 98.54,
            "rank": 28
        },
        "chemistry": {
            "marks": 120,
            "percentage": 98.25,
            "rank": 5
        },
        "maths": {
            "marks": 115,
            "percentage": 99.67,
            "rank": 77
        },
        "total": {
            "marks": 351,
            "percentage": 97.19,
            "rank": 35
        }
    },
    {
        "OMRID": 770453379,
        "name": "Ernestine Mccray",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 112,
            "percentage": 99.42,
            "rank": 6
        },
        "chemistry": {
            "marks": 117,
            "percentage": 95.34,
            "rank": 35
        },
        "maths": {
            "marks": 110,
            "percentage": 95.05,
            "rank": 47
        },
        "total": {
            "marks": 358,
            "percentage": 97.05,
            "rank": 51
        }
    },
    {
        "OMRID": 476626380,
        "name": "Guzman Hopper",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 120,
            "percentage": 99.97,
            "rank": 7
        },
        "chemistry": {
            "marks": 100,
            "percentage": 96.08,
            "rank": 74
        },
        "maths": {
            "marks": 103,
            "percentage": 95.24,
            "rank": 17
        },
        "total": {
            "marks": 317,
            "percentage": 99.92,
            "rank": 60
        }
    },
    {
        "OMRID": 270789381,
        "name": "Romero Petersen",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 103,
            "percentage": 96.98,
            "rank": 56
        },
        "chemistry": {
            "marks": 115,
            "percentage": 97.16,
            "rank": 37
        },
        "maths": {
            "marks": 100,
            "percentage": 95.56,
            "rank": 84
        },
        "total": {
            "marks": 308,
            "percentage": 99.9,
            "rank": 50
        }
    },
    {
        "OMRID": 900801382,
        "name": "Macdonald Medina",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 112,
            "percentage": 96.06,
            "rank": 38
        },
        "chemistry": {
            "marks": 113,
            "percentage": 96.2,
            "rank": 94
        },
        "maths": {
            "marks": 116,
            "percentage": 99.01,
            "rank": 21
        },
        "total": {
            "marks": 307,
            "percentage": 95.39,
            "rank": 77
        }
    },
    {
        "OMRID": 723378383,
        "name": "Keller Singleton",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 105,
            "percentage": 95.21,
            "rank": 29
        },
        "chemistry": {
            "marks": 101,
            "percentage": 98.11,
            "rank": 54
        },
        "maths": {
            "marks": 104,
            "percentage": 98.66,
            "rank": 25
        },
        "total": {
            "marks": 318,
            "percentage": 98.61,
            "rank": 32
        }
    },
    {
        "OMRID": 903382384,
        "name": "Rasmussen Gould",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 115,
            "percentage": 96.25,
            "rank": 71
        },
        "chemistry": {
            "marks": 108,
            "percentage": 96.81,
            "rank": 62
        },
        "maths": {
            "marks": 109,
            "percentage": 95.66,
            "rank": 68
        },
        "total": {
            "marks": 330,
            "percentage": 98.54,
            "rank": 26
        }
    },
    {
        "OMRID": 527380385,
        "name": "Darla Hartman",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 105,
            "percentage": 95.55,
            "rank": 12
        },
        "chemistry": {
            "marks": 103,
            "percentage": 95.95,
            "rank": 25
        },
        "maths": {
            "marks": 107,
            "percentage": 97,
            "rank": 9
        },
        "total": {
            "marks": 350,
            "percentage": 98.38,
            "rank": 56
        }
    },
    {
        "OMRID": 935909386,
        "name": "Franco Jacobson",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 104,
            "percentage": 95.35,
            "rank": 19
        },
        "chemistry": {
            "marks": 119,
            "percentage": 95.02,
            "rank": 22
        },
        "maths": {
            "marks": 106,
            "percentage": 96.65,
            "rank": 62
        },
        "total": {
            "marks": 320,
            "percentage": 96.75,
            "rank": 17
        }
    },
    {
        "OMRID": 929883387,
        "name": "Burns Leonard",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 101,
            "percentage": 96.82,
            "rank": 13
        },
        "chemistry": {
            "marks": 113,
            "percentage": 96.12,
            "rank": 46
        },
        "maths": {
            "marks": 113,
            "percentage": 99.95,
            "rank": 47
        },
        "total": {
            "marks": 338,
            "percentage": 98.34,
            "rank": 42
        }
    },
    {
        "OMRID": 138832388,
        "name": "Bonner Schultz",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 105,
            "percentage": 99.96,
            "rank": 76
        },
        "chemistry": {
            "marks": 101,
            "percentage": 95.27,
            "rank": 4
        },
        "maths": {
            "marks": 100,
            "percentage": 95.87,
            "rank": 30
        },
        "total": {
            "marks": 310,
            "percentage": 97.24,
            "rank": 2
        }
    },
    {
        "OMRID": 286697389,
        "name": "Velazquez Erickson",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 102,
            "percentage": 95.42,
            "rank": 81
        },
        "chemistry": {
            "marks": 111,
            "percentage": 97.05,
            "rank": 98
        },
        "maths": {
            "marks": 119,
            "percentage": 97.71,
            "rank": 59
        },
        "total": {
            "marks": 350,
            "percentage": 97.43,
            "rank": 36
        }
    },
    {
        "OMRID": 643663390,
        "name": "Eliza Sharpe",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 114,
            "percentage": 99.52,
            "rank": 12
        },
        "chemistry": {
            "marks": 106,
            "percentage": 95.61,
            "rank": 84
        },
        "maths": {
            "marks": 104,
            "percentage": 96.74,
            "rank": 65
        },
        "total": {
            "marks": 306,
            "percentage": 98.17,
            "rank": 99
        }
    },
    {
        "OMRID": 899198391,
        "name": "Stevenson Crane",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 100,
            "percentage": 99.45,
            "rank": 91
        },
        "chemistry": {
            "marks": 110,
            "percentage": 99.48,
            "rank": 51
        },
        "maths": {
            "marks": 108,
            "percentage": 99.87,
            "rank": 10
        },
        "total": {
            "marks": 359,
            "percentage": 97.51,
            "rank": 17
        }
    },
    {
        "OMRID": 586847392,
        "name": "Shaw Alvarado",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 105,
            "percentage": 99.76,
            "rank": 68
        },
        "chemistry": {
            "marks": 118,
            "percentage": 99.61,
            "rank": 99
        },
        "maths": {
            "marks": 107,
            "percentage": 98.85,
            "rank": 53
        },
        "total": {
            "marks": 337,
            "percentage": 98.1,
            "rank": 67
        }
    },
    {
        "OMRID": 424117393,
        "name": "Lynn Dotson",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 112,
            "percentage": 95.99,
            "rank": 88
        },
        "chemistry": {
            "marks": 106,
            "percentage": 98.86,
            "rank": 45
        },
        "maths": {
            "marks": 105,
            "percentage": 98.79,
            "rank": 15
        },
        "total": {
            "marks": 336,
            "percentage": 99.46,
            "rank": 100
        }
    },
    {
        "OMRID": 314180394,
        "name": "Stafford Bell",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 106,
            "percentage": 98.45,
            "rank": 66
        },
        "chemistry": {
            "marks": 120,
            "percentage": 96.06,
            "rank": 33
        },
        "maths": {
            "marks": 109,
            "percentage": 97.79,
            "rank": 83
        },
        "total": {
            "marks": 301,
            "percentage": 98.87,
            "rank": 49
        }
    },
    {
        "OMRID": 708620395,
        "name": "Robyn Rodgers",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 103,
            "percentage": 99.96,
            "rank": 59
        },
        "chemistry": {
            "marks": 117,
            "percentage": 98.18,
            "rank": 20
        },
        "maths": {
            "marks": 117,
            "percentage": 97.89,
            "rank": 5
        },
        "total": {
            "marks": 310,
            "percentage": 96.6,
            "rank": 8
        }
    },
    {
        "OMRID": 609812396,
        "name": "Serrano Estrada",
        "campus": "Hyderabad",
        "section": "A",
        "physics": {
            "marks": 118,
            "percentage": 98.2,
            "rank": 70
        },
        "chemistry": {
            "marks": 103,
            "percentage": 99.3,
            "rank": 47
        },
        "maths": {
            "marks": 116,
            "percentage": 97.61,
            "rank": 47
        },
        "total": {
            "marks": 329,
            "percentage": 95.21,
            "rank": 83
        }
    },
    {
        "OMRID": 907030397,
        "name": "Lilly Yang",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 112,
            "percentage": 98.08,
            "rank": 12
        },
        "chemistry": {
            "marks": 116,
            "percentage": 98.38,
            "rank": 50
        },
        "maths": {
            "marks": 114,
            "percentage": 97.21,
            "rank": 51
        },
        "total": {
            "marks": 343,
            "percentage": 96.64,
            "rank": 22
        }
    },
    {
        "OMRID": 103581398,
        "name": "Johnston Wilkerson",
        "campus": "Hyderabad",
        "section": "B",
        "physics": {
            "marks": 106,
            "percentage": 98.64,
            "rank": 48
        },
        "chemistry": {
            "marks": 101,
            "percentage": 98.32,
            "rank": 32
        },
        "maths": {
            "marks": 112,
            "percentage": 99.17,
            "rank": 44
        },
        "total": {
            "marks": 346,
            "percentage": 99.09,
            "rank": 14
        }
    },
    {
        "OMRID": 415829399,
        "name": "Cummings Wright",
        "campus": "Hyderabad",
        "section": "c",
        "physics": {
            "marks": 100,
            "percentage": 99.35,
            "rank": 26
        },
        "chemistry": {
            "marks": 111,
            "percentage": 97.87,
            "rank": 5
        },
        "maths": {
            "marks": 106,
            "percentage": 96.61,
            "rank": 43
        },
        "total": {
            "marks": 341,
            "percentage": 96.35,
            "rank": 51
        }
    }
]
