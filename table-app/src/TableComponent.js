import React, { Component } from 'react';
import './App.css';
class TableComponent extends Component {
	constructor(props){
		super(props);
		this.getFixedTableRow = this.getFixedTableRow.bind(this);
		this.getFixedTableHeaderRow = this.getFixedTableHeaderRow.bind(this);
		this.getScrollableTableRow = this.getScrollableTableRow.bind(this);
		this.getScrollableTableHeaderRow = this.getScrollableTableHeaderRow.bind(this);
		this.getScrollableTableWidth = this.getScrollableTableWidth.bind(this);

	}
	getScrollableTableWidth(columns,fixedColumns){
		var columns = this.props.columns;
		var columnsLength = columns.length;

		var size = 0;
		for(var i=this.props.fixedColumns;i<columns.length;i++){
			size+=parseInt(columns[i].width);
		}	
		return size;
	}
	parse(cx,expression){
		console.log(expression);
		var expArr = expression.split(".");
		var data = cx;
		for(var i=0;i<expArr.length;i++){
			data = data[expArr[i]];
		}
		return data;
	}
	getScrollableTableRow(row,i){
		var tds = [];
		var columns = this.props.columns;
		var columsLength = columns.length;
		for(let i=this.props.fixedColumns;i<columsLength;i++){
			tds.push(<td className={columns[i].className ||columns[i].name} style={{width:columns[i].width,height:this.props.cellHeight}}>{this.parse(row,columns[i].name)}</td>)
		}

		return (<tr key={i}>
					{tds}
				</tr>)
	}
	getFixedTableRow(row,i){
		var tds = [];
		var columns = this.props.columns;
		for(let i=0;i<this.props.fixedColumns;i++){
			tds.push(<td className={columns[i].className ||columns[i].name} style={{width:columns[i].width,height:this.props.cellHeight}}>{this.parse(row,columns[i].name)}</td>)
		}
		return (<tr key={i}>
					{tds}
				</tr>)
	}
	getScrollableTableHeaderRow(){

		var ths = [];
		var columns = this.props.columns;
		var columsLength = columns.length;
		for(let i=this.props.fixedColumns;i<columsLength;i++){
			ths.push(<th className={columns[i].className ||columns[i].name} style={{width:columns[i].width,height:this.props.headerHeight}}>{columns[i].label}</th>)
		}
		return (<tr>
					{ths}
				</tr>)
	}
	getFixedTableHeaderRow(){
		var ths = [];
		var columns = this.props.columns;
		for(let i=0;i<this.props.fixedColumns;i++){
			ths.push(<th className={columns[i].className ||columns[i].name} style={{width:columns[i].width,height:this.props.headerHeight}}>{columns[i].label}</th>)
		}
		return (<tr>
					{ths}
				</tr>)
	}
	getFixedTableWrapperWidth(){
		var size = 0;
		var columns = this.props.columns;
		for(var i=0;i<this.props.fixedColumns;i++){
			size+=parseInt(columns[i].width)
		}
		return size;
	}
	getScrollableTableWrapperWidth(){
		var size = 0;
		var columns = this.props.columns;
		for(var i=0;i<this.props.fixedColumns;i++){
			size+=parseInt(columns[i].width)
		}
		return this.props.tableWidth - size - 20;
	}
	handleScroll(e){
		var rect1 = document.getElementById("rightWrapper").getBoundingClientRect();
		var rect2 = document.getElementById("scrollableTable").getBoundingClientRect();
		var scrolledInX = rect1.left - rect2.left;
		document.getElementById("scrollableHeader").scrollTo(scrolledInX,0);
	}
	render() {
		let scrollableTableWidth = this.getScrollableTableWidth();
		let scrollableTableWrapperWidth = this.getScrollableTableWrapperWidth.call(this);
	    return (
	    <div className="main-table-cont" style={{width:this.props.tableWidth}}>
	   	<div className="table-wrapper" style={{width:this.props.tableWidth,height:this.props.tableHeight,paddingTop:this.props.headerHeight}}>
	    <div className="left-table-wrapper" style={{width:this.getFixedTableWrapperWidth.call(this)}}>	
	      <table className="custom-table table-fixed">
	      	<thead className="table-header main-header">
	      		{this.getFixedTableHeaderRow()}
	      	</thead>
	      	<tbody className="table-body">
	      		{this.props.data.map(this.getFixedTableRow)}
	      	</tbody>	
	      </table>
	    </div>
	    <div id="rightWrapper" class="right-table-wrapper" style={{width:scrollableTableWrapperWidth}} onScroll={this.handleScroll}>
	      <table id="scrollableTable" className="custom-table table-fixed" style={{width:scrollableTableWidth}}>

	      	<thead id="scrollableHeader" className="table-header main-header" style={{width:scrollableTableWrapperWidth,overflow:"hidden"}}>
	      		<div id="scrollHeaderDiv" style={{width:scrollableTableWidth,overflow:"hidden",display:"flex"}}>
	      			{this.getScrollableTableHeaderRow()}
	      		</div>
	      	</thead>
	      	<tbody className="table-body">
	      		{this.props.data.map(this.getScrollableTableRow)}
	      	</tbody>
	      </table>
	    </div>
	    </div>
	    </div>
	    );
  	}
}

export default TableComponent;
