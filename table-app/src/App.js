import React, { Component } from 'react';
import './App.css';
import {data} from "./store.js";
import TableComponent from "./TableComponent.js";
class App extends Component {
  render() {
    let columns = [
        {
          name:"OMRID",
          label:"OMRID",
          width:"100px",
          className:"omrid"
        },
        {
          name:"name",
          label:"Name",
          width:"200px",
          className:"name"
        },
        {
          name:"campus",
          label:"Campus",
          width:"100px",
          className:"campus"
        },
        {
          name:"section",
          label:"Section",
          width:"100px",
          className:"section"
        },

        {
          name:"physics.marks",
          label:"",
          width:"100px",
          className:"marks"
        },
        {
          name:"physics.percentage",
          label:"Physics",
          width:"100px",
          className:"percentage"
        },
        {
          name:"physics.rank",
          label:"",
          width:"100px",
          className:"rank"
        },
        {
          name:"chemistry.marks",
          label:"",
          width:"100px",
          className:"marks"
        },
        {
          name:"chemistry.percentage",
          label:"Chemistry",
          width:"100px",
          className:"percentage"
        },
        {
          name:"chemistry.rank",
          label:"",
          width:"100px",
          className:"rank"
        },
        {
          name:"maths.marks",
          label:"",
          width:"100px",
          className:"marks"
        },
        {
          name:"maths.percentage",
          label:"Maths",
          width:"100px",
          className:"percentage"
        },
        {
          name:"maths.rank",
          label:"",
          width:"100px",
          className:"rank"
        },
        {
          name:"total.marks",
          label:"",
          width:"100px",
          className:"marks"
        },
        {
          name:"total.percentage",
          label:"Total",
          width:"100px",
          className:"percentage"
        },
        {
          name:"total.rank",
          label:"",
          width:"100px",
          className:"rank"
        }
        ]; 
    return (
      <TableComponent 
        data={data}
        columns={columns}
        fixedColumns ={2}
        headerHeight={25}
        cellHeight={40}
        tableHeight={500}
        tableWidth={1000} />
    );
  }
}

export default App;
